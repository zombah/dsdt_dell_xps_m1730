/*
 * Intel ACPI Component Architecture
 * AML Disassembler version 20140724-32
 * Copyright (c) 2000 - 2014 Intel Corporation
 * 
 * Disassembly of dsdt.dat, Wed Apr 15 14:08:43 2015
 *
 * Original Table Header:
 *     Signature        "DSDT"
 *     Length           0x00004BD0 (19408)
 *     Revision         0x02
 *     Checksum         0x5F
 *     OEM ID           "INT430"
 *     OEM Table ID     "SYSFexxx"
 *     OEM Revision     0x00001001 (4097)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20050624 (537200164)
 */
DefinitionBlock ("dsdt.aml", "DSDT", 2, "INT430", "SYSFexxx", 0x00001001)
{
    Name (VERS, Package (0x03)
    {
        "Project: DELL D05    ", 
        "Date: 01/01/2005", 
        "Ver: 1.00.00"
    })
    Name (MISC, Buffer (0x07)
    {
         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00         /* ....... */
    })
    CreateByteField (MISC, 0x00, MIS0)
    CreateByteField (MISC, 0x01, MIS1)
    CreateByteField (MISC, 0x03, MIS3)
    CreateByteField (MISC, 0x04, MIS4)
    CreateByteField (MISC, 0x06, MIS6)
    Scope (\)
    {
        OperationRegion (RCRB, SystemMemory, 0xFED18000, 0x4000)
        Field (RCRB, DWordAcc, Lock, Preserve)
        {
            Offset (0x1000), 
            Offset (0x3000), 
            Offset (0x3404), 
            HPAS,   2, 
                ,   5, 
            HPAE,   1, 
            Offset (0x3418), 
                ,   1, 
            PATD,   1, 
            SATD,   1, 
            SMBD,   1, 
            HDAD,   1, 
            A97D,   1, 
            Offset (0x341A), 
            RPD1,   1, 
            RPD2,   1, 
            RPD3,   1, 
            RPD4,   1, 
            RPD5,   1, 
            RPD6,   1
        }
    }

    Scope (\_SB)
    {
        OperationRegion (SMIR, SystemIO, 0xB2, 0x02)
        Field (SMIR, ByteAcc, NoLock, Preserve)
        {
            SMIC,   8, 
            SMID,   8
        }

        OperationRegion (SMR2, SystemIO, 0x86, 0x01)
        Field (SMR2, ByteAcc, NoLock, Preserve)
        {
            SMIA,   8
        }

        OperationRegion (SMR3, SystemIO, 0x66, 0x01)
        Field (SMR3, ByteAcc, NoLock, Preserve)
        {
                ,   2, 
            MECI,   1, 
            Offset (0x01)
        }
    }

    Mutex (SMIX, 0x01)
    Method (SMI, 2, NotSerialized)
    {
        Acquire (SMIX, 0xFFFF)
        While (LEqual (\_SB.MECI, 0x00)) {}
        Store (Arg1, \_SB.SMIA)
        Store (Arg0, \_SB.SMIC)
        Store (\_SB.SMIC, Local0)
        While (LNotEqual (Local0, 0x00))
        {
            Store (\_SB.SMIC, Local0)
        }

        Store (\_SB.SMIA, Local1)
        Release (SMIX)
        Return (Local1)
    }

    Name (SXX0, Buffer (0x0100) {})
    Name (SXX1, Buffer (0x08) {})
    CreateWordField (SXX1, 0x00, SXX2)
    CreateWordField (SXX1, 0x04, SXX3)
    Method (SX10, 0, NotSerialized)
    {
        Acquire (SMIX, 0xFFFF)
        Store (0x00, SXX2) /* \SXX2 */
    }

    Method (SX30, 1, NotSerialized)
    {
        Store (SXX2, Local0)
        Increment (Local0)
        If (LLessEqual (Local0, SizeOf (SXX0)))
        {
            CreateByteField (SXX0, SXX2, SX20)
            Store (Arg0, SX20) /* \SX30.SX20 */
            Store (Local0, SXX2) /* \SXX2 */
        }
    }

    Method (SX31, 1, NotSerialized)
    {
        Store (SXX2, Local0)
        Add (Local0, 0x02, Local0)
        If (LLessEqual (Local0, SizeOf (SXX0)))
        {
            CreateWordField (SXX0, SXX2, SX21)
            Store (Arg0, SX21) /* \SX31.SX21 */
            Store (Local0, SXX2) /* \SXX2 */
        }
    }

    Method (SX32, 1, NotSerialized)
    {
        Store (SXX2, Local0)
        Add (Local0, 0x04, Local0)
        If (LLessEqual (Local0, SizeOf (SXX0)))
        {
            CreateDWordField (SXX0, SXX2, SX22)
            Store (Arg0, SX22) /* \SX32.SX22 */
            Store (Local0, SXX2) /* \SXX2 */
        }
    }

    Method (SX33, 2, NotSerialized)
    {
        If (LLess (Arg1, SizeOf (Arg0)))
        {
            CreateByteField (Arg0, Arg1, SX20)
            SX30 (SX20)
        }
    }

    Method (SX34, 2, NotSerialized)
    {
        Store (0x00, Local0)
        While (LLess (Local0, Arg1))
        {
            SX33 (Arg0, Local0)
            Increment (Local0)
        }
    }

    Method (SXX6, 2, NotSerialized)
    {
        Store (Arg1, \_SB.SMIA)
        Store (Arg0, \_SB.SMIC)
        Store (\_SB.SMIC, Local0)
        While (LNotEqual (Local0, 0x00))
        {
            Store (\_SB.SMIC, Local0)
        }

        Return (\_SB.SMIA)
    }

    Method (SXX5, 2, NotSerialized)
    {
        If (LLess (Arg1, SizeOf (Arg0)))
        {
            CreateByteField (Arg0, Arg1, SX20)
            SXX6 (0x7C, SX20)
        }
    }

    Method (SXX4, 0, NotSerialized)
    {
        SXX6 (0x7B, 0x00)
        Store (0x00, Local0)
        While (LLess (Local0, SXX2))
        {
            SXX5 (SXX0, Local0)
            Increment (Local0)
        }
    }

    Method (SXX8, 2, NotSerialized)
    {
        If (LLess (Arg1, SizeOf (Arg0)))
        {
            CreateByteField (Arg0, Arg1, SX20)
            Store (SXX6 (0x7D, 0x00), SX20) /* \SXX8.SX20 */
        }
    }

    Method (SXX7, 0, NotSerialized)
    {
        Store (0x00, Local0)
        While (LLess (Local0, SXX3))
        {
            Add (SXX2, Local0, Local1)
            SXX8 (SXX0, Local1)
            Increment (Local0)
        }
    }

    Method (SX11, 0, NotSerialized)
    {
        SXX4 ()
        Store (SXX6 (0x79, 0x00), SXX3) /* \SXX3 */
        Add (SXX2, SXX3, Local0)
        If (LLess (SizeOf (SXX0), Local0))
        {
            Store (SizeOf (SXX0), Local0)
            Subtract (Local0, SXX2, Local0)
            Store (Local0, SXX3) /* \SXX3 */
        }

        SXX7 ()
    }

    Method (SX40, 0, NotSerialized)
    {
        Store (SXX2, Local0)
        Increment (Local0)
        If (LLessEqual (Local0, SizeOf (SXX0)))
        {
            CreateByteField (SXX0, SXX2, SX20)
            Store (Local0, SXX2) /* \SXX2 */
            Return (SX20) /* \SX40.SX20 */
        }

        Return (0x00)
    }

    Method (SX41, 0, NotSerialized)
    {
        Store (SXX2, Local0)
        Add (Local0, 0x02, Local0)
        If (LLessEqual (Local0, SizeOf (SXX0)))
        {
            CreateWordField (SXX0, SXX2, SX21)
            Store (Local0, SXX2) /* \SXX2 */
            Return (SX21) /* \SX41.SX21 */
        }

        Return (0x00)
    }

    Method (SX42, 0, NotSerialized)
    {
        Store (SXX2, Local0)
        Add (Local0, 0x04, Local0)
        If (LLessEqual (Local0, SizeOf (SXX0)))
        {
            CreateDWordField (SXX0, SXX2, SX22)
            Store (Local0, SXX2) /* \SXX2 */
            Return (SX22) /* \SX42.SX22 */
        }

        Return (0x00)
    }

    Method (SX43, 2, NotSerialized)
    {
        If (LLess (Arg1, SizeOf (Arg0)))
        {
            CreateByteField (Arg0, Arg1, SX20)
            Store (SX40 (), SX20) /* \SX43.SX20 */
        }
    }

    Method (SX44, 2, NotSerialized)
    {
        Store (0x00, Local0)
        While (LLess (Local0, Arg1))
        {
            SX43 (Arg0, Local0)
            Increment (Local0)
        }
    }

    Method (SX45, 0, NotSerialized)
    {
        Store (SX40 (), Local0)
        Name (SX23, Buffer (Local0) {})
        SX44 (SX23, Local0)
        Return (SX23) /* \SX45.SX23 */
    }

    Method (SX12, 0, NotSerialized)
    {
        Release (SMIX)
    }

    Method (PSW, 2, NotSerialized)
    {
        SX10 ()
        SX30 (0x06)
        SX30 (Arg0)
        SX30 (Arg1)
        SX11 ()
        SX12 ()
    }

    Method (DSS, 2, NotSerialized)
    {
        SX10 ()
        SX30 (0x08)
        SX30 (Arg0)
        SX32 (Arg1)
        SX11 ()
        SX12 ()
    }

    Method (GMEM, 0, NotSerialized)
    {
        SX10 ()
        SX30 (0x07)
        SX11 ()
        Store (SX42 (), Local0)
        SX12 ()
        Return (Local0)
    }

    Method (G4GB, 0, NotSerialized)
    {
        SX10 ()
        SX30 (0x1B)
        SX11 ()
        Store (SX42 (), Local0)
        SX12 ()
        Return (Local0)
    }

    Method (SMMB, 0, NotSerialized)
    {
        SX10 ()
        SX30 (0x11)
        SX11 ()
        Store (SX42 (), Local0)
        SX12 ()
        Return (Local0)
    }

    Method (GPXB, 0, NotSerialized)
    {
        SX10 ()
        SX30 (0x1A)
        SX11 ()
        Store (SX42 (), Local0)
        SX12 ()
        Return (Local0)
    }

    Method (GORL, 0, NotSerialized)
    {
        SX10 ()
        SX30 (0x09)
        SX11 ()
        Store (SX42 (), Local0)
        SX12 ()
        Return (Local0)
    }

    Method (HSCO, 1, NotSerialized)
    {
        SX10 ()
        SX30 (0x17)
        SX30 (Arg0)
        SX11 ()
        Store (SX40 (), Local0)
        SX12 ()
        Return (Local0)
    }

    Mutex (WMIS, 0x01)
    Name (WM00, 0x00)
    Method (WM02, 0, NotSerialized)
    {
        SX10 ()
        SX30 (0x15)
        SX11 ()
        Store (SX42 (), WM00) /* \WM00 */
        SX12 ()
    }

    Method (WM03, 3, NotSerialized)
    {
        OperationRegion (WWPR, SystemMemory, Arg0, 0x01)
        Field (WWPR, ByteAcc, Lock, Preserve)
        {
            MEMW,   8
        }

        CreateByteField (Arg1, Arg2, WVAL)
        Store (WVAL, MEMW) /* \WM03.MEMW */
    }

    Method (WM05, 3, NotSerialized)
    {
        OperationRegion (WWPR, SystemMemory, Arg0, 0x04)
        Field (WWPR, ByteAcc, Lock, Preserve)
        {
            MW32,   32
        }

        CreateDWordField (Arg1, Arg2, WVAL)
        Store (WVAL, MW32) /* \WM05.MW32 */
    }

    Method (WM04, 3, NotSerialized)
    {
        OperationRegion (WRPR, SystemMemory, Arg0, 0x01)
        Field (WRPR, ByteAcc, Lock, Preserve)
        {
            MEMR,   8
        }

        CreateByteField (Arg1, Arg2, WVAL)
        Store (MEMR, WVAL) /* \WM04.WVAL */
        Store (0x00, MEMR) /* \WM04.MEMR */
    }

    Method (WM06, 3, NotSerialized)
    {
        OperationRegion (WRPR, SystemMemory, Arg0, 0x04)
        Field (WRPR, ByteAcc, Lock, Preserve)
        {
            MR32,   32
        }

        CreateDWordField (Arg1, Arg2, WVAL)
        Store (MR32, WVAL) /* \WM06.WVAL */
        Store (0x00, MR32) /* \WM06.MR32 */
    }

    Method (WM07, 2, NotSerialized)
    {
        If (LLessEqual (Arg1, 0x1000))
        {
            Store (WM00, Local0)
            Store (0x00, Local1)
            While (LLess (Local1, Arg1))
            {
                WM05 (Local0, Arg0, Local1)
                Add (Local0, 0x04, Local0)
                Add (Local1, 0x04, Local1)
            }

            SXX6 (0xCC, 0x00)
            Store (WM00, Local0)
            Store (0x00, Local1)
            While (LLess (Local1, Arg1))
            {
                WM06 (Local0, Arg0, Local1)
                Add (Local0, 0x04, Local0)
                Add (Local1, 0x04, Local1)
            }
        }

        Return (Arg0)
    }

    Method (WMI, 2, NotSerialized)
    {
        Acquire (WMIS, 0xFFFF)
        If (LEqual (WM00, 0x00))
        {
            WM02 ()
        }

        WM07 (Arg0, Arg1)
        Release (WMIS)
        Return (Arg0)
    }

    Name (W98S, "Microsoft Windows")
    Name (NT5S, "Microsoft Windows NT")
    Name (WINM, "Microsoft WindowsME: Millennium Edition")
    Name (WXP, "Windows 2001")
    Name (WLG, "Windows 2006")
    Name (LNX, "Linux")
    Method (GETC, 2, NotSerialized)
    {
        CreateByteField (Arg0, Arg1, TCHR)
        Return (TCHR) /* \GETC.TCHR */
    }

    Method (STRE, 2, NotSerialized)
    {
        Name (STR1, Buffer (0x50) {})
        Name (STR2, Buffer (0x50) {})
        Store (Arg0, STR1) /* \STRE.STR1 */
        Store (Arg1, STR2) /* \STRE.STR2 */
        Store (Zero, Local0)
        Store (One, Local1)
        While (Local1)
        {
            Store (GETC (STR1, Local0), Local1)
            Store (GETC (STR2, Local0), Local2)
            If (LNotEqual (Local1, Local2))
            {
                Return (Zero)
            }

            Increment (Local0)
        }

        Return (One)
    }

    Method (OSID, 0, NotSerialized)
    {
        If (LEqual (MIS3, 0x00))
        {
            Store (0x01, MIS3) /* \MIS3 */
            If (CondRefOf (\_OSI, Local0))
            {
                If (\_OSI (WXP))
                {
                    Store (0x10, MIS3) /* \MIS3 */
                }

                If (\_OSI (WLG))
                {
                    Store (0x20, MIS3) /* \MIS3 */
                }

                If (\_OSI (LNX))
                {
                    Store (0x40, MIS3) /* \MIS3 */
                }
            }
            Else
            {
                If (STRE (\_OS, W98S))
                {
                    Store (0x02, MIS3) /* \MIS3 */
                }

                If (STRE (\_OS, NT5S))
                {
                    Store (0x08, MIS3) /* \MIS3 */
                }

                If (STRE (\_OS, WINM))
                {
                    Store (0x04, MIS3) /* \MIS3 */
                }
            }
        }

        Return (MIS3) /* \MIS3 */
    }

    Method (SOST, 0, NotSerialized)
    {
        SX10 ()
        SX30 (0x0A)
        OSID ()
        SX30 (MIS3)
        SX11 ()
        SX12 ()
    }

    Name (\_S0, Package (0x03)  // _S0_: S0 System State
    {
        0x00, 
        0x00, 
        0x00
    })
    Name (\_S3, Package (0x03)  // _S3_: S3 System State
    {
        0x05, 
        0x00, 
        0x00
    })
    Name (\_S4, Package (0x03)  // _S4_: S4 System State
    {
        0x07, 
        0x00, 
        0x00
    })
    Name (\_S5, Package (0x03)  // _S5_: S5 System State
    {
        0x07, 
        0x00, 
        0x00
    })
    Name (WAKE, 0x00)
    Method (NEVT, 0, NotSerialized)
    {
        SX10 ()
        SX30 (0x1C)
        SX11 ()
        Store (SX41 (), Local0)
        SX12 ()
        If (And (Local0, 0x01))
        {
            Notify (\_SB.PBTN, 0x80) // Status Change
        }

        If (And (Local0, 0x04))
        {
            LIDE ()
        }

        If (And (Local0, 0x08))
        {
            PWRE ()
        }

        If (And (Local0, 0x40))
        {
            Notify (\_SB.SBTN, 0x80) // Status Change
        }

        If (And (Local0, 0x80))
        {
            SMIE ()
        }

        If (And (Local0, 0x0100))
        {
            CSC ()
        }
    }

    Method (LIDE, 0, NotSerialized)
    {
        SMI (0x43, 0x00)
        Notify (\_SB.LID, 0x80) // Status Change
    }

    Method (CSC, 0, NotSerialized)
    {
        Notify (\_SB.AMW0, 0xD0) // Hardware-Specific
    }

    Method (PWRE, 0, NotSerialized)
    {
        Store (SMI (0x98, 0x00), Local0)
        XOr (Local0, MIS0, Local1)
        And (Local0, Or (0x01, Or (0x02, 0x10)), MIS0) /* \MIS0 */
        If (And (Local1, 0x01))
        {
            Notify (\_SB.AC, 0x80) // Status Change
        }

        And (MIS0, 0x02, Local2)
        If (And (Local1, 0x02))
        {
            If (Local2)
            {
                Notify (\_SB.BAT0, 0x81) // Information Change
            }
            Else
            {
                Notify (\_SB.BAT0, 0x81) // Information Change
            }
        }

        If (And (Local1, 0x04))
        {
            If (Local2)
            {
                Notify (\_SB.BAT0, 0x80) // Status Change
            }
        }

        If (And (Local1, 0x08))
        {
            If (Local2)
            {
                Notify (\_SB.BAT0, 0x80) // Status Change
            }
        }
    }

    Method (CESM, 0, NotSerialized)
    {
        SX10 ()
        SX30 (0x18)
        SX11 ()
        Store (SX42 (), Local0)
        SX12 ()
        If (LGreaterEqual (OSID (), 0x20))
        {
            If (And (Local0, 0x01))
            {
                Notify (\_SB.MBTN, 0x80) // Status Change
            }
        }

        If (LGreaterEqual (OSID (), 0x20))
        {
            If (And (Local0, 0x04))
            {
                Notify (\_SB.PCI0.AGP.SLI.SLI0.VID.LCD, 0x86) // Device-Specific
            }

            If (And (Local0, 0x02))
            {
                Notify (\_SB.PCI0.AGP.SLI.SLI0.VID.LCD, 0x87) // Device-Specific
            }
        }
    }

    Method (SMIE, 0, NotSerialized)
    {
        Store (SMI (0x96, 0x00), Local0)
        If (And (Local0, 0x01))
        {
            Notify (\_TZ.THM, 0x80) // Thermal Status Change
        }

        If (And (Local0, 0x20))
        {
            Notify (\_SB.PCI0.AGP.SLI.SLI0.VID, 0x81) // Information Change
        }

        If (And (Local0, 0x02))
        {
            Notify (\_SB.PCI0.AGP.SLI.SLI0.VID, 0x80) // Status Change
        }

        If (And (Local0, 0x04))
        {
            CESM ()
        }

        If (And (Local0, 0x08))
        {
            Notify (\_PR.CPU0, 0x80) // Performance Capability Change
            Notify (\_PR.CPU1, 0x80) // Performance Capability Change
        }

        If (And (Local0, 0x80))
        {
            Notify (\_SB.PCI0.RP06, 0x00) // Bus Check
        }
    }

    Method (\_PTS, 1, NotSerialized)  // _PTS: Prepare To Sleep
    {
        Store (SMI (0x46, 0x00), MIS1) /* \MIS1 */
        SMI (0x8A, Arg0)
    }

    Method (\_WAK, 1, NotSerialized)  // _WAK: Wake
    {
        SMI (0x9A, Arg0)
        If (LEqual (Arg0, 0x04))
        {
            Notify (\_SB.PBTN, 0x02) // Device Wake
            Store (0x01, MIS4) /* \MIS4 */
            SOST ()
        }

        Store (SMI (0x98, 0x00), MIS0) /* \MIS0 */
        Notify (\_SB.AC, 0x80) // Status Change
        \_SB.PCI0.WKHP ()
        Store (HSCO (0x01), Local0)
        If (Local0)
        {
            If (LGreaterEqual (OSID (), 0x20))
            {
                Notify (\_SB.MBTN, 0x02) // Device Wake
                HSCO (0x02)
            }
        }

        Return (Package (0x02)
        {
            0x00, 
            0x00
        })
    }

    Method (NWAK, 0, NotSerialized)
    {
        Store (0x01, WAKE) /* \WAKE */
        Store (SMI (0x89, 0x00), Local0)
        Store (0x00, Local1)
        If (LEqual (Local0, 0x00))
        {
            Store (0x01, Local1)
        }

        If (And (Local0, 0x01))
        {
            Store (0x01, Local1)
        }

        If (And (Local0, 0x02))
        {
            LIDE ()
        }

        If (And (Local0, 0x20))
        {
            If (LEqual (OSID (), 0x02))
            {
                Store (0x01, Local1)
            }
        }

        Notify (\_SB.BAT0, 0x81) // Information Change
        If (LEqual (Local0, 0x04))
        {
            Store (0x01, Local1)
        }

        If (Local1)
        {
            Notify (\_SB.PBTN, 0x02) // Device Wake
        }

        Store (0x00, WAKE) /* \WAKE */
    }

    Scope (\_GPE)
    {
        Method (_L01, 0, NotSerialized)  // _Lxx: Level-Triggered GPE
        {
            \_SB.PCI0.POHP ()
        }

        Method (_L1C, 0, NotSerialized)  // _Lxx: Level-Triggered GPE
        {
            NEVT ()
        }

        Method (_L17, 0, NotSerialized)  // _Lxx: Level-Triggered GPE
        {
            NWAK ()
        }

        Method (_L0B, 0, NotSerialized)  // _Lxx: Level-Triggered GPE
        {
            Store (0x00, Local0)
            Notify (\_SB.PCI0, 0x02) // Device Wake
        }

        Method (_L18, 0, NotSerialized)  // _Lxx: Level-Triggered GPE
        {
            Store (0x00, Local0)
        }

        Method (_L03, 0, NotSerialized)  // _Lxx: Level-Triggered GPE
        {
            Notify (\_SB.PCI0.USB1, 0x02) // Device Wake
        }

        Method (_L04, 0, NotSerialized)  // _Lxx: Level-Triggered GPE
        {
            Notify (\_SB.PCI0.USB2, 0x02) // Device Wake
        }

        Method (_L0C, 0, NotSerialized)  // _Lxx: Level-Triggered GPE
        {
            Notify (\_SB.PCI0.USB3, 0x02) // Device Wake
        }

        Method (_L0E, 0, NotSerialized)  // _Lxx: Level-Triggered GPE
        {
            Notify (\_SB.PCI0.USB4, 0x02) // Device Wake
        }

        Method (_L05, 0, NotSerialized)  // _Lxx: Level-Triggered GPE
        {
            Notify (\_SB.PCI0.USB5, 0x02) // Device Wake
        }

        Method (_L0D, 0, NotSerialized)  // _Lxx: Level-Triggered GPE
        {
            Store (SMI (0xC6, 0x00), Local0)
            If (And (Local0, 0x01))
            {
                Notify (\_SB.PCI0.AZAL, 0x02) // Device Wake
            }

            If (And (Local0, 0x02))
            {
                Notify (\_SB.PCI0.EHCI, 0x02) // Device Wake
            }

            If (And (Local0, 0x04))
            {
                Notify (\_SB.PCI0.EHC2, 0x02) // Device Wake
            }
        }

        Method (_L09, 0, NotSerialized)  // _Lxx: Level-Triggered GPE
        {
            Store (SMI (0xC6, 0x00), Local0)
            If (And (Local0, 0x01))
            {
                Notify (\_SB.PCI0.RP06.PXS6, 0x02) // Device Wake
            }

            If (And (Local0, 0x04))
            {
                Notify (\_SB.PCI0.RP04.PXS4, 0x02) // Device Wake
            }
        }
    }

    Name (CRS0, Buffer (0x0192) {})
    CreateWordField (CRS0, 0x0190, CRS1)
    Method (CRS3, 0, NotSerialized)
    {
        Store (0x00, CRS1) /* \CRS1 */
    }

    Method (CRS4, 1, NotSerialized)
    {
        CreateByteField (CRS0, CRS1, CRS2)
        Store (Arg0, CRS2) /* \CRS4.CRS2 */
        Increment (CRS1)
    }

    Method (CRS5, 1, NotSerialized)
    {
        CreateWordField (CRS0, CRS1, CRS2)
        Store (Arg0, CRS2) /* \CRS5.CRS2 */
        Increment (CRS1)
        Increment (CRS1)
    }

    Method (CRS6, 1, NotSerialized)
    {
        CreateDWordField (CRS0, CRS1, CRS2)
        Store (Arg0, CRS2) /* \CRS6.CRS2 */
        Add (CRS1, 0x04, CRS1) /* \CRS1 */
    }

    Method (CRS7, 1, NotSerialized)
    {
        CreateQWordField (CRS0, CRS1, CRS2)
        Store (Arg0, CRS2) /* \CRS7.CRS2 */
        Add (CRS1, 0x08, CRS1) /* \CRS1 */
    }

    Method (CR_0, 3, NotSerialized)
    {
        CRS4 (0x86)
        CRS5 (0x09)
        CRS4 (Arg0)
        CRS6 (Arg1)
        CRS6 (Arg2)
    }

    Method (CR_1, 4, NotSerialized)
    {
        CRS4 (0x47)
        CRS4 (0x01)
        CRS5 (Arg0)
        CRS5 (Arg1)
        CRS4 (Arg2)
        CRS4 (Arg3)
    }

    Method (CR_2, 2, NotSerialized)
    {
        CRS4 (0x88)
        CRS5 (0x0D)
        CRS4 (0x02)
        CRS4 (0x0C)
        CRS4 (0x00)
        CRS5 (0x00)
        CRS5 (Arg0)
        Add (Arg0, Arg1, Local0)
        Decrement (Local0)
        CRS5 (Local0)
        CRS5 (0x00)
        CRS5 (Arg1)
    }

    Method (CR_3, 2, NotSerialized)
    {
        CRS4 (0x88)
        CRS5 (0x0D)
        CRS4 (0x01)
        CRS4 (0x0C)
        CRS4 (0x03)
        CRS5 (0x00)
        CRS5 (Arg0)
        Add (Arg0, Arg1, Local0)
        Decrement (Local0)
        CRS5 (Local0)
        CRS5 (0x00)
        CRS5 (Arg1)
    }

    Method (CR_5, 2, NotSerialized)
    {
        CRS4 (0x8A)
        CRS5 (0x2B)
        CRS4 (0x00)
        CRS4 (0x0C)
        CRS4 (0x03)
        CRS7 (0x00)
        CRS7 (Arg0)
        Add (Arg0, Arg1, Local0)
        Decrement (Local0)
        CRS7 (Local0)
        CRS7 (0x00)
        CRS7 (Arg1)
    }

    Method (CR_4, 2, NotSerialized)
    {
        CRS4 (0x87)
        CRS5 (0x17)
        CRS4 (0x00)
        CRS4 (0x0C)
        CRS4 (0x03)
        CRS6 (0x00)
        CRS6 (Arg0)
        Add (Arg0, Arg1, Local0)
        Decrement (Local0)
        CRS6 (Local0)
        CRS6 (0x00)
        CRS6 (Arg1)
    }

    Method (CR_6, 0, NotSerialized)
    {
        CRS5 (0x79)
        Store (CRS1, Local0)
        Subtract (0x0192, 0x02, Local1)
        While (LLess (Local0, Local1))
        {
            CRS4 (0x00)
            Increment (Local0)
        }

        Store (0x79, CRS1) /* \CRS1 */
    }

    Scope (\_PR)
    {
        Processor (CPU0, 0x00, 0x00001010, 0x06) {}
        Processor (CPU1, 0x01, 0x00001010, 0x06) {}
    }

    Scope (\_TZ)
    {
        ThermalZone (THM)
        {
            Method (_CRT, 0, NotSerialized)  // _CRT: Critical Temperature
            {
                Store (GINF (0x12), Local0)
                Return (Local0)
            }

            Method (_TMP, 0, NotSerialized)  // _TMP: Temperature
            {
                Store (GINF (0x04), Local0)
                Return (Local0)
            }

            Method (GINF, 1, NotSerialized)
            {
                SX10 ()
                SX30 (Arg0)
                SX11 ()
                Store (SX41 (), Local0)
                SX12 ()
                If (LLess (Local0, 0x0BA6))
                {
                    Store (0x0BA6, Local0)
                }

                Return (Local0)
            }
        }
    }

    Scope (\_SB)
    {
        Device (PCI0)
        {
            Name (_HID, EisaId ("PNP0A03") /* PCI Bus */)  // _HID: Hardware ID
            Name (_ADR, 0x00)  // _ADR: Address
            Name (_PRW, Package (0x02)  // _PRW: Power Resources for Wake
            {
                0x0B, 
                0x05
            })
            Method (_INI, 0, NotSerialized)  // _INI: Initialize
            {
                Store (SMI (0x98, 0x00), MIS0) /* \MIS0 */
                And (MIS0, Or (0x01, Or (0x02, 0x10)), MIS0) /* \MIS0 */
                Store (0x01, MIS4) /* \MIS4 */
                SOST ()
            }

            Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
            {
                CRS3 ()
                CR_2 (0x00, 0x0100)
                CR_3 (0x00, 0x0CF8)
                CR_1 (0x0CF8, 0x0CF8, 0x01, 0x08)
                CR_3 (0x0D00, 0xF300)
                CR_4 (0x000A0000, 0x00020000)
                Add (0x000C0000, GORL (), Local0)
                Subtract (0x000E0000, Local0, Local1)
                CR_4 (Local0, Local1)
                Add (0x00100000, GMEM (), Local0)
                Add (Local0, 0x00100000, Local0)
                Store (GPXB (), Local3)
                Subtract (Local3, Local0, Local1)
                CR_4 (Local0, Local1)
                Add (Local3, 0x04000000, Local0)
                Subtract (0xFEC00000, Local0, Local1)
                CR_4 (Local0, Local1)
                Add (0xFEC00000, 0x00010000, Local0)
                Subtract (0xFED00000, Local0, Local1)
                CR_4 (Local0, Local1)
                Add (0xFED18000, 0x4000, Local0)
                Subtract (0xFED20000, Local0, Local1)
                CR_4 (Local0, Local1)
                Add (0xFED20000, 0x00070000, Local0)
                Subtract (0xFEDA0000, Local0, Local1)
                CR_4 (Local0, Local1)
                Add (0xFEDA6000, 0x1000, Local0)
                Subtract (0xFEE00000, Local0, Local1)
                CR_4 (Local0, Local1)
                Add (0xFEE00000, 0x00010000, Local0)
                Subtract (0xFFA00000, Local0, Local1)
                CR_4 (Local0, Local1)
                Add (0xFFA00000, 0x00200000, Local0)
                Subtract (0xFFE00000, Local0, Local1)
                CR_4 (Local0, Local1)
                If (LGreaterEqual (OSID (), 0x20))
                {
                    Store (G4GB (), Local0)
                    If (LGreater (Local0, 0x00100000))
                    {
                        ShiftLeft (Local0, 0x0C, Local0)
                        Store (0xF8000000, Local1)
                        Store (0x01, Local2)
                        ShiftLeft (Local2, 0x20, Local2)
                        Add (Local1, Local2, Local1)
                        CR_5 (Local0, Local1)
                    }
                }

                CR_6 ()
                Return (CRS0) /* \CRS0 */
            }

            Device (ISAB)
            {
                Name (_ADR, 0x001F0000)  // _ADR: Address
                Device (PS2M)
                {
                    Name (_HID, EisaId ("PNP0F13"))  // _HID: Hardware ID
                    Name (CRS, ResourceTemplate ()
                    {
                        IRQNoFlags ()
                            {12}
                    })
                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Return (CRS) /* \_SB_.PCI0.ISAB.PS2M.CRS_ */
                    }
                }

                Device (KBC)
                {
                    Name (_HID, EisaId ("PNP0303") /* IBM Enhanced Keyboard (101/102-key, PS/2 Mouse) */)  // _HID: Hardware ID
                    Name (CRS, ResourceTemplate ()
                    {
                        IO (Decode16,
                            0x0060,             // Range Minimum
                            0x0060,             // Range Maximum
                            0x10,               // Alignment
                            0x01,               // Length
                            )
                        IO (Decode16,
                            0x0064,             // Range Minimum
                            0x0064,             // Range Maximum
                            0x04,               // Alignment
                            0x01,               // Length
                            )
                        IO (Decode16,
                            0x0062,             // Range Minimum
                            0x0062,             // Range Maximum
                            0x02,               // Alignment
                            0x01,               // Length
                            )
                        IO (Decode16,
                            0x0066,             // Range Minimum
                            0x0066,             // Range Maximum
                            0x06,               // Alignment
                            0x01,               // Length
                            )
                        IRQNoFlags ()
                            {1}
                    })
                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Return (CRS) /* \_SB_.PCI0.ISAB.KBC_.CRS_ */
                    }
                }

                Device (RTC)
                {
                    Name (_HID, EisaId ("PNP0B00") /* AT Real-Time Clock */)  // _HID: Hardware ID
                    Name (RT, ResourceTemplate ()
                    {
                        IO (Decode16,
                            0x0070,             // Range Minimum
                            0x0070,             // Range Maximum
                            0x10,               // Alignment
                            0x02,               // Length
                            )
                        IRQNoFlags ()
                            {8}
                        IO (Decode16,
                            0x0072,             // Range Minimum
                            0x0072,             // Range Maximum
                            0x02,               // Alignment
                            0x06,               // Length
                            )
                    })
                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Return (RT) /* \_SB_.PCI0.ISAB.RTC_.RT__ */
                    }
                }

                Device (TMR)
                {
                    Name (_HID, EisaId ("PNP0100") /* PC-class System Timer */)  // _HID: Hardware ID
                    Name (TM, ResourceTemplate ()
                    {
                        IO (Decode16,
                            0x0040,             // Range Minimum
                            0x0040,             // Range Maximum
                            0x10,               // Alignment
                            0x04,               // Length
                            )
                        IRQNoFlags ()
                            {2}
                        IO (Decode16,
                            0x0050,             // Range Minimum
                            0x0050,             // Range Maximum
                            0x10,               // Alignment
                            0x04,               // Length
                            )
                    })
                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Return (TM) /* \_SB_.PCI0.ISAB.TMR_.TM__ */
                    }
                }

                Device (SPKR)
                {
                    Name (_HID, EisaId ("PNP0800") /* Microsoft Sound System Compatible Device */)  // _HID: Hardware ID
                    Name (CRS, ResourceTemplate ()
                    {
                        IO (Decode16,
                            0x0061,             // Range Minimum
                            0x0061,             // Range Maximum
                            0x01,               // Alignment
                            0x01,               // Length
                            )
                        IO (Decode16,
                            0x0063,             // Range Minimum
                            0x0063,             // Range Maximum
                            0x01,               // Alignment
                            0x01,               // Length
                            )
                        IO (Decode16,
                            0x0065,             // Range Minimum
                            0x0065,             // Range Maximum
                            0x01,               // Alignment
                            0x01,               // Length
                            )
                        IO (Decode16,
                            0x0067,             // Range Minimum
                            0x0067,             // Range Maximum
                            0x01,               // Alignment
                            0x01,               // Length
                            )
                    })
                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Return (CRS) /* \_SB_.PCI0.ISAB.SPKR.CRS_ */
                    }
                }

                Device (MB4)
                {
                    Name (_HID, EisaId ("PNP0C01") /* System Board */)  // _HID: Hardware ID
                    Name (_UID, 0x04)  // _UID: Unique ID
                    Name (CRS, ResourceTemplate ()
                    {
                        IO (Decode16,
                            0x0C80,             // Range Minimum
                            0x0C80,             // Range Maximum
                            0x10,               // Alignment
                            0x80,               // Length
                            )
                    })
                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Return (CRS) /* \_SB_.PCI0.ISAB.MB4_.CRS_ */
                    }
                }

                Device (PIC)
                {
                    Name (_HID, EisaId ("PNP0000") /* 8259-compatible Programmable Interrupt Controller */)  // _HID: Hardware ID
                    Name (CRS, ResourceTemplate ()
                    {
                        IO (Decode16,
                            0x0024,             // Range Minimum
                            0x0024,             // Range Maximum
                            0x04,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x0028,             // Range Minimum
                            0x0028,             // Range Maximum
                            0x04,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x002C,             // Range Minimum
                            0x002C,             // Range Maximum
                            0x04,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x0030,             // Range Minimum
                            0x0030,             // Range Maximum
                            0x04,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x0034,             // Range Minimum
                            0x0034,             // Range Maximum
                            0x04,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x0038,             // Range Minimum
                            0x0038,             // Range Maximum
                            0x04,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x003C,             // Range Minimum
                            0x003C,             // Range Maximum
                            0x04,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x00A4,             // Range Minimum
                            0x00A4,             // Range Maximum
                            0x04,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x00A8,             // Range Minimum
                            0x00A8,             // Range Maximum
                            0x04,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x00AC,             // Range Minimum
                            0x00AC,             // Range Maximum
                            0x04,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x00B0,             // Range Minimum
                            0x00B0,             // Range Maximum
                            0x04,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x00B4,             // Range Minimum
                            0x00B4,             // Range Maximum
                            0x04,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x00B8,             // Range Minimum
                            0x00B8,             // Range Maximum
                            0x04,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x00BC,             // Range Minimum
                            0x00BC,             // Range Maximum
                            0x04,               // Alignment
                            0x02,               // Length
                            )
                    })
                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Return (CRS) /* \_SB_.PCI0.ISAB.PIC_.CRS_ */
                    }
                }

                Device (MAD)
                {
                    Name (_HID, EisaId ("PNP0200") /* PC-class DMA Controller */)  // _HID: Hardware ID
                    Name (CRS, ResourceTemplate ()
                    {
                        DMA (Compatibility, BusMaster, Transfer8, )
                            {4}
                        IO (Decode16,
                            0x0000,             // Range Minimum
                            0x0000,             // Range Maximum
                            0x10,               // Alignment
                            0x10,               // Length
                            )
                        IO (Decode16,
                            0x0080,             // Range Minimum
                            0x0080,             // Range Maximum
                            0x10,               // Alignment
                            0x06,               // Length
                            )
                        IO (Decode16,
                            0x0087,             // Range Minimum
                            0x0087,             // Range Maximum
                            0x01,               // Alignment
                            0x09,               // Length
                            )
                        IO (Decode16,
                            0x00C0,             // Range Minimum
                            0x00C0,             // Range Maximum
                            0x10,               // Alignment
                            0x20,               // Length
                            )
                        IO (Decode16,
                            0x0010,             // Range Minimum
                            0x0010,             // Range Maximum
                            0x10,               // Alignment
                            0x10,               // Length
                            )
                        IO (Decode16,
                            0x0090,             // Range Minimum
                            0x0090,             // Range Maximum
                            0x10,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x0093,             // Range Minimum
                            0x0093,             // Range Maximum
                            0x01,               // Alignment
                            0x0D,               // Length
                            )
                    })
                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Return (CRS) /* \_SB_.PCI0.ISAB.MAD_.CRS_ */
                    }
                }

                Device (COPR)
                {
                    Name (_HID, EisaId ("PNP0C04") /* x87-compatible Floating Point Processing Unit */)  // _HID: Hardware ID
                    Name (CRS, ResourceTemplate ()
                    {
                        IO (Decode16,
                            0x00F0,             // Range Minimum
                            0x00F0,             // Range Maximum
                            0x10,               // Alignment
                            0x10,               // Length
                            )
                        IRQNoFlags ()
                            {13}
                    })
                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Return (CRS) /* \_SB_.PCI0.ISAB.COPR.CRS_ */
                    }
                }

                Device (HPET)
                {
                    Name (_HID, EisaId ("PNP0103") /* HPET System Timer */)  // _HID: Hardware ID
                    Name (_CID, EisaId ("PNP0C01") /* System Board */)  // _CID: Compatible ID
                    Name (BUF0, ResourceTemplate ()
                    {
                        Memory32Fixed (ReadOnly,
                            0xFED00000,         // Address Base
                            0x00000400,         // Address Length
                            )
                    })
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        If (LGreaterEqual (OSID (), 0x10))
                        {
                            Return (0x0F)
                        }

                        Return (0x00)
                    }

                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Return (BUF0) /* \_SB_.PCI0.ISAB.HPET.BUF0 */
                    }
                }
            }

            Device (PCIE)
            {
                Name (_ADR, 0x001E0000)  // _ADR: Address
                Name (_PRW, Package (0x02)  // _PRW: Power Resources for Wake
                {
                    0x0B, 
                    0x04
                })
                Device (CRD0)
                {
                    Name (_ADR, 0x00010000)  // _ADR: Address
                    Method (_INI, 0, NotSerialized)  // _INI: Initialize
                    {
                        SMI (0x95, 0x04)
                    }

                    Name (_S1D, 0x00)  // _S1D: S1 Device State
                    Name (_S3D, 0x03)  // _S3D: S3 Device State
                }

                Device (CRD1)
                {
                    Name (_ADR, 0x00010005)  // _ADR: Address
                    Method (_INI, 0, NotSerialized)  // _INI: Initialize
                    {
                        SMI (0x9D, 0x04)
                    }

                    Name (_S1D, 0x00)  // _S1D: S1 Device State
                    Name (_S3D, 0x03)  // _S3D: S3 Device State
                }
            }

            Method (UCMD, 3, NotSerialized)
            {
                SX10 ()
                SX30 (0x0F)
                SX30 (Arg0)
                SX30 (Arg1)
                SX30 (Arg2)
                SX11 ()
                Store (SX40 (), Local0)
                SX12 ()
                Return (Local0)
            }

            Method (UPSW, 2, NotSerialized)
            {
                Return (UCMD (0x02, Arg0, Arg1))
            }

            Method (UPRW, 2, NotSerialized)
            {
                Return (UCMD (0x01, Arg0, Arg1))
            }

            Device (USB1)
            {
                Name (_ADR, 0x001D0000)  // _ADR: Address
                Method (_PRW, 0, NotSerialized)  // _PRW: Power Resources for Wake
                {
                    Store (UPRW (0x00, 0x00), Local0)
                    If (LEqual (Local0, 0x03))
                    {
                        Return (Package (0x02)
                        {
                            0x03, 
                            0x03
                        })
                    }

                    If (LEqual (Local0, 0x01))
                    {
                        Return (Package (0x02)
                        {
                            0x03, 
                            0x01
                        })
                    }

                    Return (Package (0x02)
                    {
                        0x03, 
                        0x00
                    })
                }

                Method (_PSW, 1, NotSerialized)  // _PSW: Power State Wake
                {
                    UPSW (Arg0, 0x00)
                }

                Device (HUB0)
                {
                    Name (_ADR, 0x00)  // _ADR: Address
                    Device (CH00)
                    {
                        Name (_ADR, 0x01)  // _ADR: Address
                    }

                    Device (CH01)
                    {
                        Name (_ADR, 0x02)  // _ADR: Address
                    }
                }
            }

            Device (USB2)
            {
                Name (_ADR, 0x001D0001)  // _ADR: Address
                Method (_PRW, 0, NotSerialized)  // _PRW: Power Resources for Wake
                {
                    Store (UPRW (0x00, 0x01), Local0)
                    If (LEqual (Local0, 0x03))
                    {
                        Return (Package (0x02)
                        {
                            0x04, 
                            0x03
                        })
                    }

                    If (LEqual (Local0, 0x01))
                    {
                        Return (Package (0x02)
                        {
                            0x04, 
                            0x01
                        })
                    }

                    Return (Package (0x02)
                    {
                        0x04, 
                        0x00
                    })
                }

                Method (_PSW, 1, NotSerialized)  // _PSW: Power State Wake
                {
                    UPSW (Arg0, 0x01)
                }

                Device (HUB1)
                {
                    Name (_ADR, 0x00)  // _ADR: Address
                    Device (CH10)
                    {
                        Name (_ADR, 0x01)  // _ADR: Address
                    }

                    Device (CH11)
                    {
                        Name (_ADR, 0x02)  // _ADR: Address
                    }
                }
            }

            Device (USB3)
            {
                Name (_ADR, 0x001D0002)  // _ADR: Address
                Method (_PRW, 0, NotSerialized)  // _PRW: Power Resources for Wake
                {
                    Store (UPRW (0x00, 0x02), Local0)
                    If (LEqual (Local0, 0x03))
                    {
                        Return (Package (0x02)
                        {
                            0x0C, 
                            0x03
                        })
                    }

                    If (LEqual (Local0, 0x01))
                    {
                        Return (Package (0x02)
                        {
                            0x0C, 
                            0x01
                        })
                    }

                    Return (Package (0x02)
                    {
                        0x0C, 
                        0x00
                    })
                }

                Method (_PSW, 1, NotSerialized)  // _PSW: Power State Wake
                {
                    UPSW (Arg0, 0x02)
                }

                Device (HUB2)
                {
                    Name (_ADR, 0x00)  // _ADR: Address
                    Device (CH20)
                    {
                        Name (_ADR, 0x01)  // _ADR: Address
                    }

                    Device (CH21)
                    {
                        Name (_ADR, 0x02)  // _ADR: Address
                    }
                }
            }

            Device (USB4)
            {
                Name (_ADR, 0x001A0000)  // _ADR: Address
                Method (_PRW, 0, NotSerialized)  // _PRW: Power Resources for Wake
                {
                    Store (UPRW (0x00, 0x03), Local0)
                    If (LEqual (Local0, 0x03))
                    {
                        Return (Package (0x02)
                        {
                            0x0E, 
                            0x03
                        })
                    }

                    If (LEqual (Local0, 0x01))
                    {
                        Return (Package (0x02)
                        {
                            0x0E, 
                            0x01
                        })
                    }

                    Return (Package (0x02)
                    {
                        0x0E, 
                        0x00
                    })
                }

                Method (_PSW, 1, NotSerialized)  // _PSW: Power State Wake
                {
                    UPSW (Arg0, 0x03)
                }

                Device (HUB3)
                {
                    Name (_ADR, 0x00)  // _ADR: Address
                    Device (CH30)
                    {
                        Name (_ADR, 0x01)  // _ADR: Address
                    }

                    Device (CH31)
                    {
                        Name (_ADR, 0x02)  // _ADR: Address
                    }
                }
            }

            Device (USB5)
            {
                Name (_ADR, 0x001A0001)  // _ADR: Address
                Method (_PRW, 0, NotSerialized)  // _PRW: Power Resources for Wake
                {
                    Store (UPRW (0x00, 0x04), Local0)
                    If (LEqual (Local0, 0x03))
                    {
                        Return (Package (0x02)
                        {
                            0x05, 
                            0x03
                        })
                    }

                    If (LEqual (Local0, 0x01))
                    {
                        Return (Package (0x02)
                        {
                            0x05, 
                            0x01
                        })
                    }

                    Return (Package (0x02)
                    {
                        0x05, 
                        0x00
                    })
                }

                Method (_PSW, 1, NotSerialized)  // _PSW: Power State Wake
                {
                    UPSW (Arg0, 0x04)
                }

                Device (HUB6)
                {
                    Name (_ADR, 0x00)  // _ADR: Address
                    Device (CH60)
                    {
                        Name (_ADR, 0x01)  // _ADR: Address
                    }

                    Device (CH61)
                    {
                        Name (_ADR, 0x02)  // _ADR: Address
                    }
                }
            }

            Device (EHC2)
            {
                Name (_ADR, 0x001A0007)  // _ADR: Address
                Name (_S1D, 0x02)  // _S1D: S1 Device State
                Name (_S3D, 0x02)  // _S3D: S3 Device State
                Method (_PRW, 0, NotSerialized)  // _PRW: Power Resources for Wake
                {
                    Store (UPRW (0x00, 0x07), Local0)
                    If (LEqual (Local0, 0x03))
                    {
                        Return (Package (0x02)
                        {
                            0x0D, 
                            0x03
                        })
                    }

                    If (LEqual (Local0, 0x01))
                    {
                        Return (Package (0x02)
                        {
                            0x0D, 
                            0x01
                        })
                    }

                    Return (Package (0x02)
                    {
                        0x0D, 
                        0x00
                    })
                }

                Method (_PSW, 1, NotSerialized)  // _PSW: Power State Wake
                {
                    UPSW (Arg0, 0x07)
                }

                Device (HUB7)
                {
                    Name (_ADR, 0x00)  // _ADR: Address
                    Device (CH00)
                    {
                        Name (_ADR, 0x01)  // _ADR: Address
                    }

                    Device (CH01)
                    {
                        Name (_ADR, 0x02)  // _ADR: Address
                    }

                    Device (CH10)
                    {
                        Name (_ADR, 0x03)  // _ADR: Address
                    }

                    Device (CH11)
                    {
                        Name (_ADR, 0x04)  // _ADR: Address
                    }
                }
            }

            Device (EHCI)
            {
                Name (_ADR, 0x001D0007)  // _ADR: Address
                Name (_S1D, 0x02)  // _S1D: S1 Device State
                Name (_S3D, 0x02)  // _S3D: S3 Device State
                Method (_PRW, 0, NotSerialized)  // _PRW: Power Resources for Wake
                {
                    Store (UPRW (0x00, 0x07), Local0)
                    If (LEqual (Local0, 0x03))
                    {
                        Return (Package (0x02)
                        {
                            0x0D, 
                            0x03
                        })
                    }

                    If (LEqual (Local0, 0x01))
                    {
                        Return (Package (0x02)
                        {
                            0x0D, 
                            0x01
                        })
                    }

                    Return (Package (0x02)
                    {
                        0x0D, 
                        0x00
                    })
                }

                Method (_PSW, 1, NotSerialized)  // _PSW: Power State Wake
                {
                    UPSW (Arg0, 0x07)
                }

                Device (HUB7)
                {
                    Name (_ADR, 0x00)  // _ADR: Address
                    Device (CH00)
                    {
                        Name (_ADR, 0x01)  // _ADR: Address
                    }

                    Device (CH01)
                    {
                        Name (_ADR, 0x02)  // _ADR: Address
                    }

                    Device (CH10)
                    {
                        Name (_ADR, 0x03)  // _ADR: Address
                    }

                    Device (CH11)
                    {
                        Name (_ADR, 0x04)  // _ADR: Address
                    }

                    Device (CH20)
                    {
                        Name (_ADR, 0x05)  // _ADR: Address
                    }

                    Device (CH21)
                    {
                        Name (_ADR, 0x06)  // _ADR: Address
                    }
                }
            }

            Device (IDE0)
            {
                Name (_ADR, 0x001F0002)  // _ADR: Address
                OperationRegion (PCFG, PCI_Config, 0x00, 0x0100)
                Field (PCFG, DWordAcc, NoLock, Preserve)
                {
                    Offset (0x40), 
                    TPF0,   1, 
                    TPI0,   1, 
                    TPP0,   1, 
                    TPD0,   1, 
                    TPF1,   1, 
                    TPI1,   1, 
                    TPP1,   1, 
                    TPD1,   1, 
                    TPRT,   2, 
                        ,   2, 
                    TPIS,   2, 
                    TPTR,   1, 
                    TPDE,   1, 
                    TSF0,   1, 
                    TSI0,   1, 
                    TSP0,   1, 
                    TSD0,   1, 
                    TSF1,   1, 
                    TSI1,   1, 
                    TSP1,   1, 
                    TSD1,   1, 
                    TSRT,   2, 
                        ,   2, 
                    TSIS,   2, 
                    TSTR,   1, 
                    TSDE,   1, 
                    PRTS,   2, 
                    PIOS,   2, 
                    SRTS,   2, 
                    SIOS,   2, 
                    Offset (0x48), 
                    SCP0,   1, 
                    SCP1,   1, 
                    SCS0,   1, 
                    SCS1,   1, 
                    Offset (0x4A), 
                    PCT0,   2, 
                        ,   2, 
                    PCT1,   2, 
                    Offset (0x4B), 
                    SCT0,   2, 
                        ,   2, 
                    SCT1,   2, 
                    Offset (0x4C), 
                    Offset (0x54), 
                    PCB0,   1, 
                    PCB1,   1, 
                    SCB0,   1, 
                    SCB1,   1, 
                        ,   2, 
                    PMCR,   1, 
                    PSCR,   1, 
                        ,   4, 
                    FPC0,   1, 
                    FPC1,   1, 
                    FSC0,   1, 
                    FSC1,   1, 
                    PSIG,   2, 
                    SSIG,   2, 
                    Offset (0x58)
                }

                Name (GTMT, Buffer (0x14)
                {
                    /* 0000 */  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,  /* ........ */
                    /* 0008 */  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,  /* ........ */
                    /* 0010 */  0x11, 0x00, 0x00, 0x00                           /* .... */
                })
                CreateDWordField (GTMT, 0x00, PIO0)
                CreateDWordField (GTMT, 0x04, DMA0)
                CreateDWordField (GTMT, 0x08, PIO1)
                CreateDWordField (GTMT, 0x0C, DMA1)
                CreateDWordField (GTMT, 0x10, IFLG)
                Method (GTMI, 0, NotSerialized)
                {
                    Store (Ones, PIO0) /* \_SB_.PCI0.IDE0.PIO0 */
                    Store (Ones, DMA0) /* \_SB_.PCI0.IDE0.DMA0 */
                    Store (Ones, PIO1) /* \_SB_.PCI0.IDE0.PIO1 */
                    Store (Ones, DMA1) /* \_SB_.PCI0.IDE0.DMA1 */
                    Store (0x10, IFLG) /* \_SB_.PCI0.IDE0.IFLG */
                }

                Method (UDMA, 3, NotSerialized)
                {
                    If (Arg0)
                    {
                        Return (0x14)
                    }

                    If (Arg1)
                    {
                        If (LEqual (Arg2, 0x02))
                        {
                            Return (0x1E)
                        }
                        Else
                        {
                            Return (0x2D)
                        }
                    }
                    Else
                    {
                        Multiply (Subtract (0x04, Arg2), 0x1E, Local0)
                        Return (Local0)
                    }
                }

                Method (PIOM, 2, NotSerialized)
                {
                    Store (0x09, Local0)
                    Subtract (Local0, Arg0, Local0)
                    Subtract (Local0, Arg1, Local0)
                    Multiply (Local0, 0x1E, Local0)
                    Return (Local0)
                }

                Method (GTMP, 0, NotSerialized)
                {
                    GTMI ()
                    If (LEqual (TPDE, 0x00))
                    {
                        Return (GTMT) /* \_SB_.PCI0.IDE0.GTMT */
                    }

                    Or (SCP0, IFLG, IFLG) /* \_SB_.PCI0.IDE0.IFLG */
                    ShiftLeft (TPI0, 0x01, Local0)
                    Or (Local0, IFLG, IFLG) /* \_SB_.PCI0.IDE0.IFLG */
                    ShiftLeft (SCP1, 0x02, Local0)
                    Or (Local0, IFLG, IFLG) /* \_SB_.PCI0.IDE0.IFLG */
                    ShiftLeft (TPI1, 0x03, Local0)
                    Or (Local0, IFLG, IFLG) /* \_SB_.PCI0.IDE0.IFLG */
                    Store (PIOM (TPIS, TPRT), PIO0) /* \_SB_.PCI0.IDE0.PIO0 */
                    Store (PIOM (PRTS, PIOS), PIO1) /* \_SB_.PCI0.IDE0.PIO1 */
                    If (SCP0)
                    {
                        Store (UDMA (FPC0, PCB0, PCT0), DMA0) /* \_SB_.PCI0.IDE0.DMA0 */
                    }
                    Else
                    {
                        If (TPD0)
                        {
                            Store (PIO0, DMA0) /* \_SB_.PCI0.IDE0.DMA0 */
                        }
                    }

                    If (SCP1)
                    {
                        Store (UDMA (FPC1, PCB1, PCT1), DMA1) /* \_SB_.PCI0.IDE0.DMA1 */
                    }
                    Else
                    {
                        If (TPD1)
                        {
                            Store (PIO1, DMA1) /* \_SB_.PCI0.IDE0.DMA1 */
                        }
                    }

                    Return (GTMT) /* \_SB_.PCI0.IDE0.GTMT */
                }

                Method (GTMS, 0, NotSerialized)
                {
                    GTMI ()
                    If (LEqual (TSDE, 0x00))
                    {
                        Return (GTMT) /* \_SB_.PCI0.IDE0.GTMT */
                    }

                    Or (SCS0, IFLG, IFLG) /* \_SB_.PCI0.IDE0.IFLG */
                    ShiftLeft (TSI0, 0x01, Local0)
                    Or (Local0, IFLG, IFLG) /* \_SB_.PCI0.IDE0.IFLG */
                    ShiftLeft (SCS1, 0x02, Local0)
                    Or (Local0, IFLG, IFLG) /* \_SB_.PCI0.IDE0.IFLG */
                    ShiftLeft (TSI1, 0x03, Local0)
                    Or (Local0, IFLG, IFLG) /* \_SB_.PCI0.IDE0.IFLG */
                    Store (PIOM (TSIS, TSRT), PIO0) /* \_SB_.PCI0.IDE0.PIO0 */
                    Store (PIOM (SRTS, SIOS), PIO1) /* \_SB_.PCI0.IDE0.PIO1 */
                    If (SCS0)
                    {
                        Store (UDMA (FSC0, SCB0, SCT0), DMA0) /* \_SB_.PCI0.IDE0.DMA0 */
                    }
                    Else
                    {
                        If (TSD0)
                        {
                            Store (PIO0, DMA0) /* \_SB_.PCI0.IDE0.DMA0 */
                        }
                    }

                    If (SCS1)
                    {
                        Store (UDMA (FSC1, SCB1, SCT1), DMA1) /* \_SB_.PCI0.IDE0.DMA1 */
                    }
                    Else
                    {
                        If (TSD1)
                        {
                            Store (PIO1, DMA1) /* \_SB_.PCI0.IDE0.DMA1 */
                        }
                    }

                    Return (GTMT) /* \_SB_.PCI0.IDE0.GTMT */
                }

                Method (_STA, 0, NotSerialized)  // _STA: Status
                {
                    Return (0x0F)
                }

                Device (PRI)
                {
                    Name (_ADR, 0x00)  // _ADR: Address
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        Return (0x0F)
                    }

                    Method (_GTM, 0, NotSerialized)  // _GTM: Get Timing Mode
                    {
                        Return (GTMP ())
                    }
                }

                Device (SEC0)
                {
                    Name (_ADR, 0x01)  // _ADR: Address
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        Return (0x0F)
                    }

                    Method (_GTM, 0, NotSerialized)  // _GTM: Get Timing Mode
                    {
                        Return (GTMS ())
                    }
                }
            }

            Device (IDE1)
            {
                Name (_ADR, 0x001F0001)  // _ADR: Address
                OperationRegion (PCFG, PCI_Config, 0x00, 0x0100)
                Field (PCFG, DWordAcc, NoLock, Preserve)
                {
                    Offset (0x40), 
                    TPF0,   1, 
                    TPI0,   1, 
                    TPP0,   1, 
                    TPD0,   1, 
                    TPF1,   1, 
                    TPI1,   1, 
                    TPP1,   1, 
                    TPD1,   1, 
                    TPRT,   2, 
                        ,   2, 
                    TPIS,   2, 
                    TPTR,   1, 
                    TPDE,   1, 
                    TSF0,   1, 
                    TSI0,   1, 
                    TSP0,   1, 
                    TSD0,   1, 
                    TSF1,   1, 
                    TSI1,   1, 
                    TSP1,   1, 
                    TSD1,   1, 
                    TSRT,   2, 
                        ,   2, 
                    TSIS,   2, 
                    TSTR,   1, 
                    TSDE,   1, 
                    PRTS,   2, 
                    PIOS,   2, 
                    SRTS,   2, 
                    SIOS,   2, 
                    Offset (0x48), 
                    SCP0,   1, 
                    SCP1,   1, 
                    SCS0,   1, 
                    SCS1,   1, 
                    Offset (0x4A), 
                    PCT0,   2, 
                        ,   2, 
                    PCT1,   2, 
                    Offset (0x4B), 
                    SCT0,   2, 
                        ,   2, 
                    SCT1,   2, 
                    Offset (0x4C), 
                    Offset (0x54), 
                    PCB0,   1, 
                    PCB1,   1, 
                    SCB0,   1, 
                    SCB1,   1, 
                        ,   2, 
                    PMCR,   1, 
                    PSCR,   1, 
                        ,   4, 
                    FPC0,   1, 
                    FPC1,   1, 
                    FSC0,   1, 
                    FSC1,   1, 
                    PSIG,   2, 
                    SSIG,   2, 
                    Offset (0x58)
                }

                Name (GTMT, Buffer (0x14)
                {
                    /* 0000 */  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,  /* ........ */
                    /* 0008 */  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,  /* ........ */
                    /* 0010 */  0x11, 0x00, 0x00, 0x00                           /* .... */
                })
                CreateDWordField (GTMT, 0x00, PIO0)
                CreateDWordField (GTMT, 0x04, DMA0)
                CreateDWordField (GTMT, 0x08, PIO1)
                CreateDWordField (GTMT, 0x0C, DMA1)
                CreateDWordField (GTMT, 0x10, IFLG)
                Method (GTMI, 0, NotSerialized)
                {
                    Store (Ones, PIO0) /* \_SB_.PCI0.IDE1.PIO0 */
                    Store (Ones, DMA0) /* \_SB_.PCI0.IDE1.DMA0 */
                    Store (Ones, PIO1) /* \_SB_.PCI0.IDE1.PIO1 */
                    Store (Ones, DMA1) /* \_SB_.PCI0.IDE1.DMA1 */
                    Store (0x10, IFLG) /* \_SB_.PCI0.IDE1.IFLG */
                }

                Method (UDMA, 3, NotSerialized)
                {
                    If (Arg0)
                    {
                        Return (0x14)
                    }

                    If (Arg1)
                    {
                        If (LEqual (Arg2, 0x02))
                        {
                            Return (0x1E)
                        }
                        Else
                        {
                            Return (0x2D)
                        }
                    }
                    Else
                    {
                        Multiply (Subtract (0x04, Arg2), 0x1E, Local0)
                        Return (Local0)
                    }
                }

                Method (PIOM, 2, NotSerialized)
                {
                    Store (0x09, Local0)
                    Subtract (Local0, Arg0, Local0)
                    Subtract (Local0, Arg1, Local0)
                    Multiply (Local0, 0x1E, Local0)
                    Return (Local0)
                }

                Method (GTMP, 0, NotSerialized)
                {
                    GTMI ()
                    If (LEqual (TPDE, 0x00))
                    {
                        Return (GTMT) /* \_SB_.PCI0.IDE1.GTMT */
                    }

                    Or (SCP0, IFLG, IFLG) /* \_SB_.PCI0.IDE1.IFLG */
                    ShiftLeft (TPI0, 0x01, Local0)
                    Or (Local0, IFLG, IFLG) /* \_SB_.PCI0.IDE1.IFLG */
                    ShiftLeft (SCP1, 0x02, Local0)
                    Or (Local0, IFLG, IFLG) /* \_SB_.PCI0.IDE1.IFLG */
                    ShiftLeft (TPI1, 0x03, Local0)
                    Or (Local0, IFLG, IFLG) /* \_SB_.PCI0.IDE1.IFLG */
                    Store (PIOM (TPIS, TPRT), PIO0) /* \_SB_.PCI0.IDE1.PIO0 */
                    Store (PIOM (PRTS, PIOS), PIO1) /* \_SB_.PCI0.IDE1.PIO1 */
                    If (SCP0)
                    {
                        Store (UDMA (FPC0, PCB0, PCT0), DMA0) /* \_SB_.PCI0.IDE1.DMA0 */
                    }
                    Else
                    {
                        If (TPD0)
                        {
                            Store (PIO0, DMA0) /* \_SB_.PCI0.IDE1.DMA0 */
                        }
                    }

                    If (SCP1)
                    {
                        Store (UDMA (FPC1, PCB1, PCT1), DMA1) /* \_SB_.PCI0.IDE1.DMA1 */
                    }
                    Else
                    {
                        If (TPD1)
                        {
                            Store (PIO1, DMA1) /* \_SB_.PCI0.IDE1.DMA1 */
                        }
                    }

                    Return (GTMT) /* \_SB_.PCI0.IDE1.GTMT */
                }

                Method (GTMS, 0, NotSerialized)
                {
                    GTMI ()
                    If (LEqual (TSDE, 0x00))
                    {
                        Return (GTMT) /* \_SB_.PCI0.IDE1.GTMT */
                    }

                    Or (SCS0, IFLG, IFLG) /* \_SB_.PCI0.IDE1.IFLG */
                    ShiftLeft (TSI0, 0x01, Local0)
                    Or (Local0, IFLG, IFLG) /* \_SB_.PCI0.IDE1.IFLG */
                    ShiftLeft (SCS1, 0x02, Local0)
                    Or (Local0, IFLG, IFLG) /* \_SB_.PCI0.IDE1.IFLG */
                    ShiftLeft (TSI1, 0x03, Local0)
                    Or (Local0, IFLG, IFLG) /* \_SB_.PCI0.IDE1.IFLG */
                    Store (PIOM (TSIS, TSRT), PIO0) /* \_SB_.PCI0.IDE1.PIO0 */
                    Store (PIOM (SRTS, SIOS), PIO1) /* \_SB_.PCI0.IDE1.PIO1 */
                    If (SCS0)
                    {
                        Store (UDMA (FSC0, SCB0, SCT0), DMA0) /* \_SB_.PCI0.IDE1.DMA0 */
                    }
                    Else
                    {
                        If (TSD0)
                        {
                            Store (PIO0, DMA0) /* \_SB_.PCI0.IDE1.DMA0 */
                        }
                    }

                    If (SCS1)
                    {
                        Store (UDMA (FSC1, SCB1, SCT1), DMA1) /* \_SB_.PCI0.IDE1.DMA1 */
                    }
                    Else
                    {
                        If (TSD1)
                        {
                            Store (PIO1, DMA1) /* \_SB_.PCI0.IDE1.DMA1 */
                        }
                    }

                    Return (GTMT) /* \_SB_.PCI0.IDE1.GTMT */
                }

                Method (_STA, 0, NotSerialized)  // _STA: Status
                {
                    Return (0x0F)
                }

                Device (PRI)
                {
                    Name (_ADR, 0x00)  // _ADR: Address
                    Method (_GTM, 0, NotSerialized)  // _GTM: Get Timing Mode
                    {
                        Return (GTMP ())
                    }

                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        Return (0x0F)
                    }
                }

                Device (SEC0)
                {
                    Name (_ADR, 0x01)  // _ADR: Address
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        Return (0x0F)
                    }

                    Method (_GTM, 0, NotSerialized)  // _GTM: Get Timing Mode
                    {
                        Return (GTMS ())
                    }
                }
            }

            Device (AZAL)
            {
                Name (_ADR, 0x001B0000)  // _ADR: Address
                Name (_PRW, Package (0x02)  // _PRW: Power Resources for Wake
                {
                    0x0D, 
                    0x03
                })
            }

            Device (AGP)
            {
                Name (_ADR, 0x00010000)  // _ADR: Address
                Device (SLI)
                {
                    Name (_ADR, 0x00)  // _ADR: Address
                    Device (SLI0)
                    {
                        Name (_ADR, 0x00)  // _ADR: Address
                        Device (VID)
                        {
                            Name (_ADR, 0x00)  // _ADR: Address
                            Method (_DOS, 1, NotSerialized)  // _DOS: Disable Output Switching
                            {
                                Store (Arg0, MIS4) /* \MIS4 */
                                SMI (0x9E, MIS4)
                            }

                            Method (_DOD, 0, NotSerialized)  // _DOD: Display Output Devices
                            {
                                Store (SMI (0x6D, 0x00), Local0)
                                If (LEqual (Local0, 0x01))
                                {
                                    Return (Package (0x04)
                                    {
                                        0x00010100, 
                                        0x00010200, 
                                        0x00010110, 
                                        0x00010210
                                    })
                                }
                                Else
                                {
                                    Return (Package (0x04)
                                    {
                                        0x00010100, 
                                        0x00010200, 
                                        0x00010118, 
                                        0x00010120
                                    })
                                }
                            }

                            Device (TV)
                            {
                                Method (_ADR, 0, NotSerialized)  // _ADR: Address
                                {
                                    Return (0x0200)
                                }

                                Method (_DCS, 0, NotSerialized)  // _DCS: Display Current Status
                                {
                                    Store (SMI (0x8E, 0x04), Local0)
                                    Return (Local0)
                                }

                                Method (_DGS, 0, NotSerialized)  // _DGS: Display Graphics State
                                {
                                    Store (SMI (0x99, 0x04), Local0)
                                    Return (Local0)
                                }

                                Method (_DSS, 1, NotSerialized)  // _DSS: Device Set State
                                {
                                    DSS (0x04, Arg0)
                                }
                            }

                            Device (CRT)
                            {
                                Method (_ADR, 0, NotSerialized)  // _ADR: Address
                                {
                                    Return (0x0100)
                                }

                                Method (_DCS, 0, NotSerialized)  // _DCS: Display Current Status
                                {
                                    Store (SMI (0x8E, 0x02), Local0)
                                    Return (Local0)
                                }

                                Method (_DGS, 0, NotSerialized)  // _DGS: Display Graphics State
                                {
                                    Store (SMI (0x99, 0x02), Local0)
                                    Return (Local0)
                                }

                                Method (_DSS, 1, NotSerialized)  // _DSS: Device Set State
                                {
                                    DSS (0x02, Arg0)
                                }
                            }

                            Device (LCD)
                            {
                                Method (_ADR, 0, NotSerialized)  // _ADR: Address
                                {
                                    Store (SMI (0x6D, 0x00), Local0)
                                    If (LEqual (Local0, 0x01))
                                    {
                                        Return (0x0110)
                                    }
                                    Else
                                    {
                                        Return (0x0118)
                                    }
                                }

                                Method (_DCS, 0, NotSerialized)  // _DCS: Display Current Status
                                {
                                    Store (SMI (0x8E, 0x01), Local0)
                                    Return (Local0)
                                }

                                Method (_DGS, 0, NotSerialized)  // _DGS: Display Graphics State
                                {
                                    Store (SMI (0x99, 0x01), Local0)
                                    Return (Local0)
                                }

                                Method (_DSS, 1, NotSerialized)  // _DSS: Device Set State
                                {
                                    DSS (0x01, Arg0)
                                }

                                Name (BTVL, 0x64)
                                Name (DBCL, Package (0x0A) {})
                                Method (_BCL, 0, NotSerialized)  // _BCL: Brightness Control Levels
                                {
                                    SX10 ()
                                    SX30 (0x19)
                                    SX30 (0x00)
                                    SX11 ()
                                    Store (SX40 (), Index (DBCL, 0x00))
                                    Store (SX40 (), Index (DBCL, 0x01))
                                    Store (SX40 (), Index (DBCL, 0x02))
                                    Store (SX40 (), Index (DBCL, 0x03))
                                    Store (SX40 (), Index (DBCL, 0x04))
                                    Store (SX40 (), Index (DBCL, 0x05))
                                    Store (SX40 (), Index (DBCL, 0x06))
                                    Store (SX40 (), Index (DBCL, 0x07))
                                    Store (SX40 (), Index (DBCL, 0x08))
                                    Store (SX40 (), Index (DBCL, 0x09))
                                    SX12 ()
                                    Return (DBCL) /* \_SB_.PCI0.AGP_.SLI_.SLI0.VID_.LCD_.DBCL */
                                }

                                Method (_BCM, 1, NotSerialized)  // _BCM: Brightness Control Method
                                {
                                    SX10 ()
                                    SX30 (0x19)
                                    SX30 (0x01)
                                    SX30 (Arg0)
                                    Store (Arg0, BTVL) /* \_SB_.PCI0.AGP_.SLI_.SLI0.VID_.LCD_.BTVL */
                                    SX11 ()
                                    SX12 ()
                                }

                                Method (_BQC, 0, NotSerialized)  // _BQC: Brightness Query Current
                                {
                                    SX10 ()
                                    SX30 (0x19)
                                    SX30 (0x02)
                                    SX11 ()
                                    Store (SX40 (), Local0)
                                    Store (Local0, BTVL) /* \_SB_.PCI0.AGP_.SLI_.SLI0.VID_.LCD_.BTVL */
                                    SX12 ()
                                    Return (Local0)
                                }
                            }

                            Device (DVI)
                            {
                                Method (_ADR, 0, NotSerialized)  // _ADR: Address
                                {
                                    Store (SMI (0x6D, 0x00), Local0)
                                    If (LEqual (Local0, 0x01))
                                    {
                                        Return (0x0210)
                                    }
                                    Else
                                    {
                                        Return (0x0120)
                                    }
                                }

                                Method (_DCS, 0, NotSerialized)  // _DCS: Display Current Status
                                {
                                    Store (SMI (0x8E, 0x08), Local0)
                                    Return (Local0)
                                }

                                Method (_DGS, 0, NotSerialized)  // _DGS: Display Graphics State
                                {
                                    Store (SMI (0x99, 0x08), Local0)
                                    Return (Local0)
                                }

                                Method (_DSS, 1, NotSerialized)  // _DSS: Device Set State
                                {
                                    DSS (0x08, Arg0)
                                }
                            }
                        }
                    }

                    Device (SLI1)
                    {
                        Name (_ADR, 0x00010000)  // _ADR: Address
                        Device (VID1)
                        {
                            Name (_ADR, 0x00)  // _ADR: Address
                            Method (_DOS, 1, NotSerialized)  // _DOS: Disable Output Switching
                            {
                            }

                            Method (_DOD, 0, NotSerialized)  // _DOD: Display Output Devices
                            {
                                Return (Package (0x00) {})
                            }
                        }
                    }
                }
            }

            Scope (\_SB)
            {
                Device (MB1)
                {
                    Name (_HID, EisaId ("PNP0C01") /* System Board */)  // _HID: Hardware ID
                    Name (_UID, 0x01)  // _UID: Unique ID
                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        CRS3 ()
                        CR_0 (0x01, 0x00, 0x0009F000)
                        CR_0 (0x01, 0x0009F000, 0x1000)
                        If (LGreaterEqual (OSID (), 0x08))
                        {
                            Store (GORL (), Local0)
                            CR_0 (0x00, 0x000C0000, Local0)
                        }

                        CR_0 (0x00, 0x000E0000, 0x00020000)
                        Store (GMEM (), Local0)
                        Subtract (Local0, 0x0008E000, Local1)
                        CR_0 (0x01, 0x00100000, Local1)
                        Add (Local1, 0x00100000, Local1)
                        CR_0 (0x01, Local1, 0x0008E000)
                        Store (SMMB (), Local0)
                        CR_0 (0x01, Local0, 0x00100000)
                        CR_0 (0x00, 0xFFE00000, 0x00200000)
                        CR_0 (0x00, 0xFFA00000, 0x00200000)
                        CR_0 (0x01, 0xFEC00000, 0x00010000)
                        CR_0 (0x01, 0xFEE00000, 0x00010000)
                        CR_0 (0x00, 0xFED20000, 0x00070000)
                        CR_0 (0x01, 0xFEDA0000, 0x4000)
                        CR_0 (0x01, 0xFEDA4000, 0x1000)
                        CR_0 (0x01, 0xFEDA5000, 0x1000)
                        CR_0 (0x01, 0xFEDA6000, 0x1000)
                        CR_0 (0x01, 0xFED1C800, 0x0800)
                        CR_0 (0x01, 0xFED18000, 0x4000)
                        Store (GPXB (), Local0)
                        CR_0 (0x01, Local0, 0x04000000)
                        CR_6 ()
                        Return (CRS0) /* \CRS0 */
                    }
                }
            }

            Scope (\_SB.PCI0)
            {
                Device (MB2)
                {
                    Name (_HID, EisaId ("PNP0C01") /* System Board */)  // _HID: Hardware ID
                    Name (_UID, 0x02)  // _UID: Unique ID
                    Name (CRS, ResourceTemplate ()
                    {
                        IO (Decode16,
                            0x0900,             // Range Minimum
                            0x0900,             // Range Maximum
                            0x01,               // Alignment
                            0x80,               // Length
                            )
                        IO (Decode16,
                            0x0092,             // Range Minimum
                            0x0092,             // Range Maximum
                            0x02,               // Alignment
                            0x01,               // Length
                            )
                        IO (Decode16,
                            0x00B2,             // Range Minimum
                            0x00B2,             // Range Maximum
                            0x02,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x0020,             // Range Minimum
                            0x0020,             // Range Maximum
                            0x10,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x00A0,             // Range Minimum
                            0x00A0,             // Range Maximum
                            0x10,               // Alignment
                            0x02,               // Length
                            )
                        IRQNoFlags ()
                            {0}
                        IO (Decode16,
                            0x04D0,             // Range Minimum
                            0x04D0,             // Range Maximum
                            0x10,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x1000,             // Range Minimum
                            0x1000,             // Range Maximum
                            0x10,               // Alignment
                            0x06,               // Length
                            )
                        IO (Decode16,
                            0x1008,             // Range Minimum
                            0x1008,             // Range Maximum
                            0x08,               // Alignment
                            0x08,               // Length
                            )
                    })
                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Return (CRS) /* \_SB_.PCI0.MB2_.CRS_ */
                    }
                }

                Device (MB3)
                {
                    Name (_HID, EisaId ("PNP0C01") /* System Board */)  // _HID: Hardware ID
                    Name (_UID, 0x03)  // _UID: Unique ID
                    Name (CRS, ResourceTemplate ()
                    {
                        IO (Decode16,
                            0xF400,             // Range Minimum
                            0xF400,             // Range Maximum
                            0x01,               // Alignment
                            0xFF,               // Length
                            )
                        IO (Decode16,
                            0x0086,             // Range Minimum
                            0x0086,             // Range Maximum
                            0x02,               // Alignment
                            0x01,               // Length
                            )
                        IO (Decode16,
                            0x1006,             // Range Minimum
                            0x1006,             // Range Maximum
                            0x02,               // Alignment
                            0x02,               // Length
                            )
                        IO (Decode16,
                            0x100A,             // Range Minimum
                            0x100A,             // Range Maximum
                            0x01,               // Alignment
                            0x50,               // Length
                            )
                        IO (Decode16,
                            0x1080,             // Range Minimum
                            0x1080,             // Range Maximum
                            0x10,               // Alignment
                            0x40,               // Length
                            )
                        IO (Decode16,
                            0x10C0,             // Range Minimum
                            0x10C0,             // Range Maximum
                            0x10,               // Alignment
                            0x20,               // Length
                            )
                        IO (Decode16,
                            0x1010,             // Range Minimum
                            0x1010,             // Range Maximum
                            0x10,               // Alignment
                            0x20,               // Length
                            )
                        IO (Decode16,
                            0x0809,             // Range Minimum
                            0x0809,             // Range Maximum
                            0x01,               // Alignment
                            0x01,               // Length
                            )
                    })
                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Return (CRS) /* \_SB_.PCI0.MB3_.CRS_ */
                    }
                }
            }

            Scope (\_SB)
            {
            }

            Device (RP01)
            {
                Name (_ADR, 0x001C0000)  // _ADR: Address
                OperationRegion (P1CS, PCI_Config, 0x00, 0x0100)
                Field (P1CS, AnyAcc, NoLock, WriteAsZeros)
                {
                    Offset (0x3E), 
                        ,   6, 
                    SBSR,   1, 
                    Offset (0x52), 
                        ,   13, 
                    LSTS,   1, 
                    Offset (0x5A), 
                    ABP1,   1, 
                        ,   2, 
                    PDC1,   1, 
                        ,   2, 
                    PDS1,   1, 
                    Offset (0x5B), 
                    LASC,   1, 
                    Offset (0x60), 
                    Offset (0x62), 
                    PSP1,   1, 
                    Offset (0xDC), 
                        ,   30, 
                    HPCS,   1, 
                    PMCS,   1, 
                    Offset (0xE2), 
                    Offset (0xE3), 
                    WXME,   2
                }

                Device (PXS1)
                {
                    Name (_ADR, 0x00)  // _ADR: Address
                }

                Name (_PRW, Package (0x02)  // _PRW: Power Resources for Wake
                {
                    0x09, 
                    0x03
                })
                Method (_PRT, 0, NotSerialized)  // _PRT: PCI Routing Table
                {
                    If (\GPIC)
                    {
                        Return (Package (0x04)
                        {
                            Package (0x04)
                            {
                                0xFFFF, 
                                0x00, 
                                0x00, 
                                0x10
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x01, 
                                0x00, 
                                0x11
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x02, 
                                0x00, 
                                0x12
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x03, 
                                0x00, 
                                0x13
                            }
                        })
                    }
                    Else
                    {
                        Return (Package (0x04)
                        {
                            Package (0x04)
                            {
                                0xFFFF, 
                                0x00, 
                                \_SB.PCI0.LNKA, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x01, 
                                \_SB.PCI0.LNKB, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x02, 
                                \_SB.PCI0.LNKC, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x03, 
                                \_SB.PCI0.LNKD, 
                                0x00
                            }
                        })
                    }
                }
            }

            Device (RP02)
            {
                Name (_ADR, 0x001C0001)  // _ADR: Address
                OperationRegion (P2CS, PCI_Config, 0x00, 0x0100)
                Field (P2CS, AnyAcc, NoLock, WriteAsZeros)
                {
                    Offset (0x3E), 
                        ,   6, 
                    SBSR,   1, 
                    Offset (0x52), 
                        ,   13, 
                    LSTS,   1, 
                    Offset (0x5A), 
                    ABP2,   1, 
                        ,   2, 
                    PDC2,   1, 
                        ,   2, 
                    PDS2,   1, 
                    Offset (0x5B), 
                    LASC,   1, 
                    Offset (0x60), 
                    Offset (0x62), 
                    PSP2,   1, 
                    Offset (0xDC), 
                        ,   30, 
                    HPCS,   1, 
                    PMCS,   1, 
                    Offset (0xE2), 
                    Offset (0xE3), 
                    WXME,   2
                }

                Device (PXS2)
                {
                    Name (_ADR, 0x00)  // _ADR: Address
                }

                Name (_PRW, Package (0x02)  // _PRW: Power Resources for Wake
                {
                    0x09, 
                    0x03
                })
                Method (_PRT, 0, NotSerialized)  // _PRT: PCI Routing Table
                {
                    If (\GPIC)
                    {
                        Return (Package (0x04)
                        {
                            Package (0x04)
                            {
                                0xFFFF, 
                                0x00, 
                                0x00, 
                                0x11
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x01, 
                                0x00, 
                                0x12
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x02, 
                                0x00, 
                                0x13
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x03, 
                                0x00, 
                                0x10
                            }
                        })
                    }
                    Else
                    {
                        Return (Package (0x04)
                        {
                            Package (0x04)
                            {
                                0xFFFF, 
                                0x00, 
                                \_SB.PCI0.LNKB, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x01, 
                                \_SB.PCI0.LNKC, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x02, 
                                \_SB.PCI0.LNKD, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x03, 
                                \_SB.PCI0.LNKA, 
                                0x00
                            }
                        })
                    }
                }
            }

            Device (RP03)
            {
                Name (_ADR, 0x001C0002)  // _ADR: Address
                OperationRegion (P3CS, PCI_Config, 0x00, 0x0100)
                Field (P3CS, AnyAcc, NoLock, WriteAsZeros)
                {
                    Offset (0x3E), 
                        ,   6, 
                    SBSR,   1, 
                    Offset (0x52), 
                        ,   13, 
                    LSTS,   1, 
                    Offset (0x5A), 
                    ABP3,   1, 
                        ,   2, 
                    PDC3,   1, 
                        ,   2, 
                    PDS3,   1, 
                    Offset (0x5B), 
                    LASC,   1, 
                    Offset (0x60), 
                    Offset (0x62), 
                    PSP3,   1, 
                    Offset (0xDC), 
                        ,   30, 
                    HPCS,   1, 
                    PMCS,   1, 
                    Offset (0xE2), 
                    Offset (0xE3), 
                    WXME,   2
                }

                Device (PXS3)
                {
                    Name (_ADR, 0x00)  // _ADR: Address
                }

                Name (_PRW, Package (0x02)  // _PRW: Power Resources for Wake
                {
                    0x09, 
                    0x03
                })
                Method (_PRT, 0, NotSerialized)  // _PRT: PCI Routing Table
                {
                    If (\GPIC)
                    {
                        Return (Package (0x04)
                        {
                            Package (0x04)
                            {
                                0xFFFF, 
                                0x00, 
                                0x00, 
                                0x12
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x01, 
                                0x00, 
                                0x13
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x02, 
                                0x00, 
                                0x10
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x03, 
                                0x00, 
                                0x11
                            }
                        })
                    }
                    Else
                    {
                        Return (Package (0x04)
                        {
                            Package (0x04)
                            {
                                0xFFFF, 
                                0x00, 
                                \_SB.PCI0.LNKC, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x01, 
                                \_SB.PCI0.LNKD, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x02, 
                                \_SB.PCI0.LNKA, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x03, 
                                \_SB.PCI0.LNKB, 
                                0x00
                            }
                        })
                    }
                }
            }

            Device (RP04)
            {
                Name (_ADR, 0x001C0003)  // _ADR: Address
                OperationRegion (P4CS, PCI_Config, 0x00, 0x0100)
                Field (P4CS, AnyAcc, NoLock, WriteAsZeros)
                {
                    Offset (0x3E), 
                        ,   6, 
                    SBSR,   1, 
                    Offset (0x52), 
                        ,   13, 
                    LSTS,   1, 
                    Offset (0x5A), 
                    ABP4,   1, 
                        ,   2, 
                    PDC4,   1, 
                        ,   2, 
                    PDS4,   1, 
                    Offset (0x5B), 
                    LASC,   1, 
                    Offset (0x60), 
                    Offset (0x62), 
                    PSP4,   1, 
                    Offset (0xDC), 
                        ,   30, 
                    HPCS,   1, 
                    PMCS,   1, 
                    Offset (0xE2), 
                    Offset (0xE3), 
                    WXME,   2
                }

                Device (PXS4)
                {
                    Name (_ADR, 0x00)  // _ADR: Address
                }

                Name (_PRW, Package (0x02)  // _PRW: Power Resources for Wake
                {
                    0x09, 
                    0x03
                })
                Method (_PRT, 0, NotSerialized)  // _PRT: PCI Routing Table
                {
                    If (\GPIC)
                    {
                        Return (Package (0x04)
                        {
                            Package (0x04)
                            {
                                0xFFFF, 
                                0x00, 
                                0x00, 
                                0x13
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x01, 
                                0x00, 
                                0x10
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x02, 
                                0x00, 
                                0x11
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x03, 
                                0x00, 
                                0x12
                            }
                        })
                    }
                    Else
                    {
                        Return (Package (0x04)
                        {
                            Package (0x04)
                            {
                                0xFFFF, 
                                0x00, 
                                \_SB.PCI0.LNKD, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x01, 
                                \_SB.PCI0.LNKA, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x02, 
                                \_SB.PCI0.LNKB, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x03, 
                                \_SB.PCI0.LNKC, 
                                0x00
                            }
                        })
                    }
                }
            }

            Device (RP05)
            {
                Name (_ADR, 0x001C0004)  // _ADR: Address
                OperationRegion (P5CS, PCI_Config, 0x00, 0x0100)
                Field (P5CS, AnyAcc, NoLock, WriteAsZeros)
                {
                    Offset (0x3E), 
                        ,   6, 
                    SBSR,   1, 
                    Offset (0x52), 
                        ,   13, 
                    LSTS,   1, 
                    Offset (0x5A), 
                    ABP5,   1, 
                        ,   2, 
                    PDC5,   1, 
                        ,   2, 
                    PDS5,   1, 
                    Offset (0x5B), 
                    LASC,   1, 
                    Offset (0x60), 
                    Offset (0x62), 
                    PSP5,   1, 
                    Offset (0xDC), 
                        ,   30, 
                    HPCS,   1, 
                    PMCS,   1, 
                    Offset (0xE2), 
                    Offset (0xE3), 
                    WXME,   2
                }

                Device (PXS5)
                {
                    Name (_ADR, 0x00)  // _ADR: Address
                }

                Name (_PRW, Package (0x02)  // _PRW: Power Resources for Wake
                {
                    0x09, 
                    0x03
                })
                Method (_PRT, 0, NotSerialized)  // _PRT: PCI Routing Table
                {
                    If (\GPIC)
                    {
                        Return (Package (0x04)
                        {
                            Package (0x04)
                            {
                                0xFFFF, 
                                0x00, 
                                0x00, 
                                0x10
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x01, 
                                0x00, 
                                0x11
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x02, 
                                0x00, 
                                0x12
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x03, 
                                0x00, 
                                0x13
                            }
                        })
                    }
                    Else
                    {
                        Return (Package (0x04)
                        {
                            Package (0x04)
                            {
                                0xFFFF, 
                                0x00, 
                                \_SB.PCI0.LNKA, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x01, 
                                \_SB.PCI0.LNKB, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x02, 
                                \_SB.PCI0.LNKC, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x03, 
                                \_SB.PCI0.LNKD, 
                                0x00
                            }
                        })
                    }
                }
            }

            Device (RP06)
            {
                Name (_ADR, 0x001C0005)  // _ADR: Address
                OperationRegion (P6CS, PCI_Config, 0x00, 0x0100)
                Field (P6CS, AnyAcc, NoLock, WriteAsZeros)
                {
                    Offset (0x3E), 
                        ,   6, 
                    SBSR,   1, 
                    Offset (0x52), 
                        ,   13, 
                    LSTS,   1, 
                    Offset (0x5A), 
                    ABP6,   1, 
                        ,   2, 
                    PDC6,   1, 
                        ,   2, 
                    PDS6,   1, 
                    Offset (0x5B), 
                    LASC,   1, 
                    Offset (0x60), 
                    Offset (0x62), 
                    PSP6,   1, 
                    Offset (0xDC), 
                        ,   30, 
                    HPCS,   1, 
                    PMCS,   1, 
                    Offset (0xE2), 
                    Offset (0xE3), 
                    WXME,   2
                }

                Device (PXS6)
                {
                    Name (_ADR, 0x00)  // _ADR: Address
                }

                Name (_PRW, Package (0x02)  // _PRW: Power Resources for Wake
                {
                    0x09, 
                    0x03
                })
                Method (_PRT, 0, NotSerialized)  // _PRT: PCI Routing Table
                {
                    If (\GPIC)
                    {
                        Return (Package (0x04)
                        {
                            Package (0x04)
                            {
                                0xFFFF, 
                                0x00, 
                                0x00, 
                                0x11
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x01, 
                                0x00, 
                                0x12
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x02, 
                                0x00, 
                                0x13
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x03, 
                                0x00, 
                                0x10
                            }
                        })
                    }
                    Else
                    {
                        Return (Package (0x04)
                        {
                            Package (0x04)
                            {
                                0xFFFF, 
                                0x00, 
                                \_SB.PCI0.LNKB, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x01, 
                                \_SB.PCI0.LNKC, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x02, 
                                \_SB.PCI0.LNKD, 
                                0x00
                            }, 

                            Package (0x04)
                            {
                                0xFFFF, 
                                0x03, 
                                \_SB.PCI0.LNKA, 
                                0x00
                            }
                        })
                    }
                }
            }

            Method (WKHP, 0, NotSerialized)
            {
                Notify (\_SB.PCI0.RP04, 0x00) // Bus Check
            }

            Method (POHP, 0, NotSerialized)
            {
                If (LEqual (0x02, 0x02))
                {
                    Sleep (0x64)
                    If (\_SB.PCI0.RP02.HPCS)
                    {
                        If (LAnd (LEqual (RPD2, 0x00), \_SB.PCI0.RP02.PDC2))
                        {
                            While (\_SB.PCI0.RP02.SBSR)
                            {
                                Sleep (0x0A)
                            }

                            Sleep (0x0A)
                            SX10 ()
                            SX30 (0x14)
                            SX11 ()
                            SX12 ()
                            Store (0x01, \_SB.PCI0.RP02.PDC2)
                            Store (0x01, \_SB.PCI0.RP02.HPCS)
                            Sleep (0x0A)
                            If (\_SB.PCI0.RP02.PDS2)
                            {
                                Notify (\_SB.PCI0.RP02, 0x00) // Bus Check
                            }
                        }
                        Else
                        {
                            Store (0x01, \_SB.PCI0.RP02.HPCS)
                        }
                    }
                }
                Else
                {
                    Sleep (0x01F4)
                    If (\_SB.PCI0.RP02.HPCS)
                    {
                        If (LAnd (LEqual (RPD2, 0x00), \_SB.PCI0.RP02.PDC2))
                        {
                            Store (0x01, \_SB.PCI0.RP02.PDC2)
                            Store (0x01, \_SB.PCI0.RP02.HPCS)
                            Notify (\_SB.PCI0.RP02, 0x00) // Bus Check
                        }
                        Else
                        {
                            Store (0x01, \_SB.PCI0.RP02.HPCS)
                        }
                    }
                }

                If (LEqual (0x02, 0x04))
                {
                    Sleep (0x64)
                    If (\_SB.PCI0.RP04.HPCS)
                    {
                        If (LAnd (LEqual (RPD4, 0x00), \_SB.PCI0.RP04.PDC4))
                        {
                            While (\_SB.PCI0.RP04.SBSR)
                            {
                                Sleep (0x0A)
                            }

                            Sleep (0x0A)
                            SX10 ()
                            SX30 (0x14)
                            SX11 ()
                            SX12 ()
                            Store (0x01, \_SB.PCI0.RP04.PDC4)
                            Store (0x01, \_SB.PCI0.RP04.HPCS)
                            Sleep (0x0A)
                            If (\_SB.PCI0.RP04.PDS4)
                            {
                                Notify (\_SB.PCI0.RP04, 0x00) // Bus Check
                            }
                        }
                        Else
                        {
                            Store (0x01, \_SB.PCI0.RP04.HPCS)
                        }
                    }
                }
                Else
                {
                    Sleep (0x01F4)
                    If (\_SB.PCI0.RP04.HPCS)
                    {
                        If (LAnd (LEqual (RPD4, 0x00), \_SB.PCI0.RP04.PDC4))
                        {
                            Store (0x01, \_SB.PCI0.RP04.PDC4)
                            Store (0x01, \_SB.PCI0.RP04.HPCS)
                            Notify (\_SB.PCI0.RP04, 0x00) // Bus Check
                        }
                        Else
                        {
                            Store (0x01, \_SB.PCI0.RP04.HPCS)
                        }
                    }
                }
            }

            Scope (\_SB.PCI0.RP04.PXS4)
            {
                Name (_EJD, "\\_SB.PCI0.EHC2.HUB7.CH00")  // _EJD: Ejection Dependent Device
                Method (_RMV, 0, NotSerialized)  // _RMV: Removal Status
                {
                    Return (0x01)
                }
            }

            Scope (\_SB.PCI0.EHC2.HUB7.CH00)
            {
                Name (_EJD, "\\_SB.PCI0.RP04.PXS4")  // _EJD: Ejection Dependent Device
            }

            Scope (\_SB.PCI0.USB4.HUB3.CH30)
            {
                Name (_EJD, "\\_SB.PCI0.RP04.PXS4")  // _EJD: Ejection Dependent Device
            }

            Scope (\_SB.PCI0.RP04)
            {
                Method (_DSM, 4, NotSerialized)  // _DSM: Device-Specific Method
                {
                    If (LEqual (Arg0, ToUUID ("e5c937d0-3553-4d7a-9117-ea4d19c3434d") /* Device Labeling Interface */))
                    {
                        If (LEqual (Arg2, 0x00))
                        {
                            Return (Buffer (0x01)
                            {
                                 0x21                                             /* ! */
                            })
                        }

                        If (LEqual (Arg2, 0x05))
                        {
                            Return (0x01)
                        }
                    }

                    Return (Buffer (0x01)
                    {
                         0x00                                             /* . */
                    })
                }
            }

            Scope (\_SB.PCI0.RP06.PXS6)
            {
                Name (LHDL, 0x00)
                Name (SLLP, Package (0x02)
                {
                    0x80000000, 
                    0x80000000
                })
                Method (LLTB, 0, NotSerialized)
                {
                    SX10 ()
                    SX30 (0x10)
                    SX11 ()
                    Store (SX42 (), Index (SLLP, 0x00))
                    Store (SX42 (), Index (SLLP, 0x01))
                    SX12 ()
                    OperationRegion (LOPR, SystemMemory, DerefOf (Index (SLLP, 0x00)), DerefOf (Index (SLLP, 0x01
                        )))
                    If (LEqual (LHDL, 0x00))
                    {
                        Load (LOPR, LHDL) /* \_SB_.PCI0.RP06.PXS6.LHDL */
                    }
                }

                Method (_INI, 0, NotSerialized)  // _INI: Initialize
                {
                    If (LGreaterEqual (OSID (), 0x10))
                    {
                        LLTB ()
                    }
                }
            }

            Scope (\)
            {
                Name (GPIC, 0x00)
                Method (_PIC, 1, NotSerialized)  // _PIC: Interrupt Model
                {
                    Store (Arg0, GPIC) /* \GPIC */
                }
            }

            Scope (\_SB.PCI0)
            {
                Scope (\_SB.PCI0.ISAB)
                {
                    OperationRegion (PIR1, PCI_Config, 0x60, 0x04)
                    OperationRegion (PIR2, PCI_Config, 0x68, 0x04)
                    OperationRegion (FDIS, PCI_Config, 0xF2, 0x02)
                }

                Name (PIC0, Package (0x16)
                {
                    Package (0x04)
                    {
                        0x001FFFFF, 
                        0x00, 
                        \_SB.PCI0.LNKC, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001FFFFF, 
                        0x01, 
                        \_SB.PCI0.LNKB, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001FFFFF, 
                        0x02, 
                        \_SB.PCI0.LNKC, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001FFFFF, 
                        0x03, 
                        \_SB.PCI0.LNKD, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001EFFFF, 
                        0x00, 
                        \_SB.PCI0.LNKA, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001EFFFF, 
                        0x01, 
                        \_SB.PCI0.LNKB, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001EFFFF, 
                        0x02, 
                        \_SB.PCI0.LNKC, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001EFFFF, 
                        0x03, 
                        \_SB.PCI0.LNKD, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001DFFFF, 
                        0x00, 
                        \_SB.PCI0.LNKE, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001DFFFF, 
                        0x01, 
                        \_SB.PCI0.LNKF, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001DFFFF, 
                        0x02, 
                        \_SB.PCI0.LNKG, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001DFFFF, 
                        0x03, 
                        \_SB.PCI0.LNKH, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001AFFFF, 
                        0x00, 
                        \_SB.PCI0.LNKE, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001AFFFF, 
                        0x01, 
                        \_SB.PCI0.LNKF, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001AFFFF, 
                        0x02, 
                        \_SB.PCI0.LNKG, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001AFFFF, 
                        0x03, 
                        \_SB.PCI0.LNKH, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001CFFFF, 
                        0x00, 
                        \_SB.PCI0.LNKA, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001CFFFF, 
                        0x01, 
                        \_SB.PCI0.LNKB, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001CFFFF, 
                        0x02, 
                        \_SB.PCI0.LNKC, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001CFFFF, 
                        0x03, 
                        \_SB.PCI0.LNKD, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x001BFFFF, 
                        0x00, 
                        \_SB.PCI0.LNKF, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x0001FFFF, 
                        0x00, 
                        \_SB.PCI0.LNKA, 
                        0x00
                    }
                })
                Name (API0, Package (0x16)
                {
                    Package (0x04)
                    {
                        0x001FFFFF, 
                        0x00, 
                        0x00, 
                        0x10
                    }, 

                    Package (0x04)
                    {
                        0x001FFFFF, 
                        0x01, 
                        0x00, 
                        0x11
                    }, 

                    Package (0x04)
                    {
                        0x001FFFFF, 
                        0x02, 
                        0x00, 
                        0x12
                    }, 

                    Package (0x04)
                    {
                        0x001FFFFF, 
                        0x03, 
                        0x00, 
                        0x13
                    }, 

                    Package (0x04)
                    {
                        0x001EFFFF, 
                        0x00, 
                        0x00, 
                        0x10
                    }, 

                    Package (0x04)
                    {
                        0x001EFFFF, 
                        0x01, 
                        0x00, 
                        0x11
                    }, 

                    Package (0x04)
                    {
                        0x001EFFFF, 
                        0x02, 
                        0x00, 
                        0x12
                    }, 

                    Package (0x04)
                    {
                        0x001EFFFF, 
                        0x03, 
                        0x00, 
                        0x13
                    }, 

                    Package (0x04)
                    {
                        0x001DFFFF, 
                        0x00, 
                        0x00, 
                        0x14
                    }, 

                    Package (0x04)
                    {
                        0x001DFFFF, 
                        0x01, 
                        0x00, 
                        0x15
                    }, 

                    Package (0x04)
                    {
                        0x001DFFFF, 
                        0x02, 
                        0x00, 
                        0x16
                    }, 

                    Package (0x04)
                    {
                        0x001DFFFF, 
                        0x03, 
                        0x00, 
                        0x17
                    }, 

                    Package (0x04)
                    {
                        0x001AFFFF, 
                        0x00, 
                        0x00, 
                        0x14
                    }, 

                    Package (0x04)
                    {
                        0x001AFFFF, 
                        0x01, 
                        0x00, 
                        0x15
                    }, 

                    Package (0x04)
                    {
                        0x001AFFFF, 
                        0x02, 
                        0x00, 
                        0x16
                    }, 

                    Package (0x04)
                    {
                        0x001AFFFF, 
                        0x03, 
                        0x00, 
                        0x17
                    }, 

                    Package (0x04)
                    {
                        0x001CFFFF, 
                        0x00, 
                        0x00, 
                        0x10
                    }, 

                    Package (0x04)
                    {
                        0x001CFFFF, 
                        0x01, 
                        0x00, 
                        0x11
                    }, 

                    Package (0x04)
                    {
                        0x001CFFFF, 
                        0x02, 
                        0x00, 
                        0x12
                    }, 

                    Package (0x04)
                    {
                        0x001CFFFF, 
                        0x03, 
                        0x00, 
                        0x13
                    }, 

                    Package (0x04)
                    {
                        0x001BFFFF, 
                        0x00, 
                        0x00, 
                        0x15
                    }, 

                    Package (0x04)
                    {
                        0x0001FFFF, 
                        0x00, 
                        0x00, 
                        0x10
                    }
                })
                Method (_PRT, 0, NotSerialized)  // _PRT: PCI Routing Table
                {
                    Store (API0, Local0)
                    If (LNot (GPIC))
                    {
                        Store (PIC0, Local0)
                    }

                    Return (Local0)
                }

                Field (\_SB.PCI0.ISAB.PIR1, ByteAcc, NoLock, Preserve)
                {
                    PIRA,   8, 
                    PIRB,   8, 
                    PIRC,   8, 
                    PIRD,   8
                }

                Field (\_SB.PCI0.ISAB.PIR2, ByteAcc, NoLock, Preserve)
                {
                    PIRE,   8, 
                    PIRF,   8, 
                    PIRG,   8, 
                    PIRH,   8
                }

                Device (LNKA)
                {
                    Name (_HID, EisaId ("PNP0C0F") /* PCI Interrupt Link Device */)  // _HID: Hardware ID
                    Name (_UID, 0x01)  // _UID: Unique ID
                    Name (_PRS, ResourceTemplate ()  // _PRS: Possible Resource Settings
                    {
                        IRQ (Level, ActiveLow, Shared, )
                            {9,10,11}
                    })
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        Store (PIRA, Local0)
                        And (Local0, 0x80, Local0)
                        If (LEqual (Local0, 0x80))
                        {
                            Return (0x09)
                        }

                        Return (0x0B)
                    }

                    Method (_DIS, 0, NotSerialized)  // _DIS: Disable Device
                    {
                        Store (PIRA, Local0)
                        Or (Local0, 0x80, Local0)
                        Store (Local0, PIRA) /* \_SB_.PCI0.PIRA */
                    }

                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Name (BUFA, ResourceTemplate ()
                        {
                            IRQ (Level, ActiveLow, Shared, _Y00)
                                {}
                        })
                        CreateWordField (BUFA, \_SB.PCI0.LNKA._CRS._Y00._INT, IRA)  // _INT: Interrupts
                        Store (PIRA, Local0)
                        And (Local0, 0x8F, Local0)
                        If (LLess (Local0, 0x80))
                        {
                            And (Local0, 0x0F)
                            Store (0x01, Local1)
                            ShiftLeft (Local1, Local0, Local1)
                            Store (Local1, IRA) /* \_SB_.PCI0.LNKA._CRS.IRA_ */
                        }

                        Return (BUFA) /* \_SB_.PCI0.LNKA._CRS.BUFA */
                    }

                    Method (_SRS, 1, NotSerialized)  // _SRS: Set Resource Settings
                    {
                        CreateWordField (Arg0, 0x01, IRQA)
                        FindSetLeftBit (IRQA, Local0)
                        Decrement (Local0)
                        Store (Local0, PIRA) /* \_SB_.PCI0.PIRA */
                    }
                }

                Device (LNKB)
                {
                    Name (_HID, EisaId ("PNP0C0F") /* PCI Interrupt Link Device */)  // _HID: Hardware ID
                    Name (_UID, 0x02)  // _UID: Unique ID
                    Name (_PRS, ResourceTemplate ()  // _PRS: Possible Resource Settings
                    {
                        IRQ (Level, ActiveLow, Shared, )
                            {5,7}
                    })
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        Store (PIRB, Local0)
                        And (Local0, 0x80, Local0)
                        If (LEqual (Local0, 0x80))
                        {
                            Return (0x09)
                        }

                        Return (0x0B)
                    }

                    Method (_DIS, 0, NotSerialized)  // _DIS: Disable Device
                    {
                        Store (PIRB, Local0)
                        Or (Local0, 0x80, Local0)
                        Store (Local0, PIRB) /* \_SB_.PCI0.PIRB */
                    }

                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Name (BUFB, ResourceTemplate ()
                        {
                            IRQ (Level, ActiveLow, Shared, _Y01)
                                {}
                        })
                        CreateWordField (BUFB, \_SB.PCI0.LNKB._CRS._Y01._INT, IRB)  // _INT: Interrupts
                        Store (PIRB, Local0)
                        And (Local0, 0x8F, Local0)
                        If (LLess (Local0, 0x80))
                        {
                            And (Local0, 0x0F)
                            Store (0x01, Local1)
                            ShiftLeft (Local1, Local0, Local1)
                            Store (Local1, IRB) /* \_SB_.PCI0.LNKB._CRS.IRB_ */
                        }

                        Return (BUFB) /* \_SB_.PCI0.LNKB._CRS.BUFB */
                    }

                    Method (_SRS, 1, NotSerialized)  // _SRS: Set Resource Settings
                    {
                        CreateWordField (Arg0, 0x01, IRQB)
                        FindSetLeftBit (IRQB, Local0)
                        Decrement (Local0)
                        Store (Local0, PIRB) /* \_SB_.PCI0.PIRB */
                    }
                }

                Device (LNKC)
                {
                    Name (_HID, EisaId ("PNP0C0F") /* PCI Interrupt Link Device */)  // _HID: Hardware ID
                    Name (_UID, 0x03)  // _UID: Unique ID
                    Name (_PRS, ResourceTemplate ()  // _PRS: Possible Resource Settings
                    {
                        IRQ (Level, ActiveLow, Shared, )
                            {9,10,11}
                    })
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        Store (PIRC, Local0)
                        And (Local0, 0x80, Local0)
                        If (LEqual (Local0, 0x80))
                        {
                            Return (0x09)
                        }

                        Return (0x0B)
                    }

                    Method (_DIS, 0, NotSerialized)  // _DIS: Disable Device
                    {
                        Store (PIRC, Local0)
                        Or (Local0, 0x80, Local0)
                        Store (Local0, PIRC) /* \_SB_.PCI0.PIRC */
                    }

                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Name (BUFC, ResourceTemplate ()
                        {
                            IRQ (Level, ActiveLow, Shared, _Y02)
                                {}
                        })
                        CreateWordField (BUFC, \_SB.PCI0.LNKC._CRS._Y02._INT, IRC)  // _INT: Interrupts
                        Store (PIRC, Local0)
                        And (Local0, 0x8F, Local0)
                        If (LLess (Local0, 0x80))
                        {
                            And (Local0, 0x0F)
                            Store (0x01, Local1)
                            ShiftLeft (Local1, Local0, Local1)
                            Store (Local1, IRC) /* \_SB_.PCI0.LNKC._CRS.IRC_ */
                        }

                        Return (BUFC) /* \_SB_.PCI0.LNKC._CRS.BUFC */
                    }

                    Method (_SRS, 1, NotSerialized)  // _SRS: Set Resource Settings
                    {
                        CreateWordField (Arg0, 0x01, IRQC)
                        FindSetLeftBit (IRQC, Local0)
                        Decrement (Local0)
                        Store (Local0, PIRC) /* \_SB_.PCI0.PIRC */
                    }
                }

                Device (LNKD)
                {
                    Name (_HID, EisaId ("PNP0C0F") /* PCI Interrupt Link Device */)  // _HID: Hardware ID
                    Name (_UID, 0x04)  // _UID: Unique ID
                    Name (_PRS, ResourceTemplate ()  // _PRS: Possible Resource Settings
                    {
                        IRQ (Level, ActiveLow, Shared, )
                            {5,7,9,10,11}
                    })
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        Store (PIRD, Local0)
                        And (Local0, 0x80, Local0)
                        If (LEqual (Local0, 0x80))
                        {
                            Return (0x09)
                        }

                        Return (0x0B)
                    }

                    Method (_DIS, 0, NotSerialized)  // _DIS: Disable Device
                    {
                        Store (PIRD, Local0)
                        Or (Local0, 0x80, Local0)
                        Store (Local0, PIRD) /* \_SB_.PCI0.PIRD */
                    }

                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Name (BUFD, ResourceTemplate ()
                        {
                            IRQ (Level, ActiveLow, Shared, _Y03)
                                {}
                        })
                        CreateWordField (BUFD, \_SB.PCI0.LNKD._CRS._Y03._INT, IRD)  // _INT: Interrupts
                        Store (PIRD, Local0)
                        And (Local0, 0x8F, Local0)
                        If (LLess (Local0, 0x80))
                        {
                            And (Local0, 0x0F)
                            Store (0x01, Local1)
                            ShiftLeft (Local1, Local0, Local1)
                            Store (Local1, IRD) /* \_SB_.PCI0.LNKD._CRS.IRD_ */
                        }

                        Return (BUFD) /* \_SB_.PCI0.LNKD._CRS.BUFD */
                    }

                    Method (_SRS, 1, NotSerialized)  // _SRS: Set Resource Settings
                    {
                        CreateWordField (Arg0, 0x01, IRQD)
                        FindSetLeftBit (IRQD, Local0)
                        Decrement (Local0)
                        Store (Local0, PIRD) /* \_SB_.PCI0.PIRD */
                    }
                }

                Device (LNKE)
                {
                    Name (_HID, EisaId ("PNP0C0F") /* PCI Interrupt Link Device */)  // _HID: Hardware ID
                    Name (_UID, 0x05)  // _UID: Unique ID
                    Name (_PRS, ResourceTemplate ()  // _PRS: Possible Resource Settings
                    {
                        IRQ (Level, ActiveLow, Shared, )
                            {3,4,5,6,7,9,10,11,12,14,15}
                    })
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        Store (PIRE, Local0)
                        And (Local0, 0x80, Local0)
                        If (LEqual (Local0, 0x80))
                        {
                            Return (0x09)
                        }

                        Return (0x0B)
                    }

                    Method (_DIS, 0, NotSerialized)  // _DIS: Disable Device
                    {
                        Store (PIRE, Local0)
                        Or (Local0, 0x80, Local0)
                        Store (Local0, PIRE) /* \_SB_.PCI0.PIRE */
                    }

                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Name (BUFE, ResourceTemplate ()
                        {
                            IRQ (Level, ActiveLow, Shared, _Y04)
                                {}
                        })
                        CreateWordField (BUFE, \_SB.PCI0.LNKE._CRS._Y04._INT, IRE)  // _INT: Interrupts
                        Store (PIRE, Local0)
                        And (Local0, 0x8F, Local0)
                        If (LLess (Local0, 0x80))
                        {
                            And (Local0, 0x0F)
                            Store (0x01, Local1)
                            ShiftLeft (Local1, Local0, Local1)
                            Store (Local1, IRE) /* \_SB_.PCI0.LNKE._CRS.IRE_ */
                        }

                        Return (BUFE) /* \_SB_.PCI0.LNKE._CRS.BUFE */
                    }

                    Method (_SRS, 1, NotSerialized)  // _SRS: Set Resource Settings
                    {
                        CreateWordField (Arg0, 0x01, IRQE)
                        FindSetLeftBit (IRQE, Local0)
                        Decrement (Local0)
                        Store (Local0, PIRE) /* \_SB_.PCI0.PIRE */
                    }
                }

                Device (LNKF)
                {
                    Name (_HID, EisaId ("PNP0C0F") /* PCI Interrupt Link Device */)  // _HID: Hardware ID
                    Name (_UID, 0x06)  // _UID: Unique ID
                    Name (_PRS, ResourceTemplate ()  // _PRS: Possible Resource Settings
                    {
                        IRQ (Level, ActiveLow, Shared, )
                            {3,4,5,6,7,9,10,11,12,14,15}
                    })
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        Store (PIRF, Local0)
                        And (Local0, 0x80, Local0)
                        If (LEqual (Local0, 0x80))
                        {
                            Return (0x09)
                        }

                        Return (0x0B)
                    }

                    Method (_DIS, 0, NotSerialized)  // _DIS: Disable Device
                    {
                        Store (PIRF, Local0)
                        Or (Local0, 0x80, Local0)
                        Store (Local0, PIRF) /* \_SB_.PCI0.PIRF */
                    }

                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Name (BUFF, ResourceTemplate ()
                        {
                            IRQ (Level, ActiveLow, Shared, _Y05)
                                {}
                        })
                        CreateWordField (BUFF, \_SB.PCI0.LNKF._CRS._Y05._INT, IRF)  // _INT: Interrupts
                        Store (PIRF, Local0)
                        And (Local0, 0x8F, Local0)
                        If (LLess (Local0, 0x80))
                        {
                            And (Local0, 0x0F)
                            Store (0x01, Local1)
                            ShiftLeft (Local1, Local0, Local1)
                            Store (Local1, IRF) /* \_SB_.PCI0.LNKF._CRS.IRF_ */
                        }

                        Return (BUFF) /* \_SB_.PCI0.LNKF._CRS.BUFF */
                    }

                    Method (_SRS, 1, NotSerialized)  // _SRS: Set Resource Settings
                    {
                        CreateWordField (Arg0, 0x01, IRQF)
                        FindSetLeftBit (IRQF, Local0)
                        Decrement (Local0)
                        Store (Local0, PIRF) /* \_SB_.PCI0.PIRF */
                    }
                }

                Device (LNKG)
                {
                    Name (_HID, EisaId ("PNP0C0F") /* PCI Interrupt Link Device */)  // _HID: Hardware ID
                    Name (_UID, 0x07)  // _UID: Unique ID
                    Name (_PRS, ResourceTemplate ()  // _PRS: Possible Resource Settings
                    {
                        IRQ (Level, ActiveLow, Shared, )
                            {3,4,5,6,7,9,10,11,12,14,15}
                    })
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        Store (PIRG, Local0)
                        And (Local0, 0x80, Local0)
                        If (LEqual (Local0, 0x80))
                        {
                            Return (0x09)
                        }

                        Return (0x0B)
                    }

                    Method (_DIS, 0, NotSerialized)  // _DIS: Disable Device
                    {
                        Store (PIRG, Local0)
                        Or (Local0, 0x80, Local0)
                        Store (Local0, PIRG) /* \_SB_.PCI0.PIRG */
                    }

                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Name (BUFG, ResourceTemplate ()
                        {
                            IRQ (Level, ActiveLow, Shared, _Y06)
                                {}
                        })
                        CreateWordField (BUFG, \_SB.PCI0.LNKG._CRS._Y06._INT, IRG)  // _INT: Interrupts
                        Store (PIRG, Local0)
                        And (Local0, 0x8F, Local0)
                        If (LLess (Local0, 0x80))
                        {
                            And (Local0, 0x0F)
                            Store (0x01, Local1)
                            ShiftLeft (Local1, Local0, Local1)
                            Store (Local1, IRG) /* \_SB_.PCI0.LNKG._CRS.IRG_ */
                        }

                        Return (BUFG) /* \_SB_.PCI0.LNKG._CRS.BUFG */
                    }

                    Method (_SRS, 1, NotSerialized)  // _SRS: Set Resource Settings
                    {
                        CreateWordField (Arg0, 0x01, IRQG)
                        FindSetLeftBit (IRQG, Local0)
                        Decrement (Local0)
                        Store (Local0, PIRG) /* \_SB_.PCI0.PIRG */
                    }
                }

                Device (LNKH)
                {
                    Name (_HID, EisaId ("PNP0C0F") /* PCI Interrupt Link Device */)  // _HID: Hardware ID
                    Name (_UID, 0x08)  // _UID: Unique ID
                    Name (_PRS, ResourceTemplate ()  // _PRS: Possible Resource Settings
                    {
                        IRQ (Level, ActiveLow, Shared, )
                            {3,4,5,6,7,9,10,11,12,14,15}
                    })
                    Method (_STA, 0, NotSerialized)  // _STA: Status
                    {
                        Store (PIRH, Local0)
                        And (Local0, 0x80, Local0)
                        If (LEqual (Local0, 0x80))
                        {
                            Return (0x09)
                        }

                        Return (0x0B)
                    }

                    Method (_DIS, 0, NotSerialized)  // _DIS: Disable Device
                    {
                        Store (PIRH, Local0)
                        Or (Local0, 0x80, Local0)
                        Store (Local0, PIRH) /* \_SB_.PCI0.PIRH */
                    }

                    Method (_CRS, 0, NotSerialized)  // _CRS: Current Resource Settings
                    {
                        Name (BUFH, ResourceTemplate ()
                        {
                            IRQ (Level, ActiveLow, Shared, _Y07)
                                {}
                        })
                        CreateWordField (BUFH, \_SB.PCI0.LNKH._CRS._Y07._INT, IRH)  // _INT: Interrupts
                        Store (PIRH, Local0)
                        And (Local0, 0x8F, Local0)
                        If (LLess (Local0, 0x80))
                        {
                            And (Local0, 0x0F)
                            Store (0x01, Local1)
                            ShiftLeft (Local1, Local0, Local1)
                            Store (Local1, IRH) /* \_SB_.PCI0.LNKH._CRS.IRH_ */
                        }

                        Return (BUFH) /* \_SB_.PCI0.LNKH._CRS.BUFH */
                    }

                    Method (_SRS, 1, NotSerialized)  // _SRS: Set Resource Settings
                    {
                        CreateWordField (Arg0, 0x01, IRQH)
                        FindSetLeftBit (IRQH, Local0)
                        Decrement (Local0)
                        Store (Local0, PIRH) /* \_SB_.PCI0.PIRH */
                    }
                }
            }

            Scope (\_SB.PCI0.AGP)
            {
                Name (PIC1, Package (0x01)
                {
                    Package (0x04)
                    {
                        0xFFFF, 
                        0x00, 
                        \_SB.PCI0.LNKA, 
                        0x00
                    }
                })
                Name (API1, Package (0x01)
                {
                    Package (0x04)
                    {
                        0xFFFF, 
                        0x00, 
                        0x00, 
                        0x10
                    }
                })
                Method (_PRT, 0, NotSerialized)  // _PRT: PCI Routing Table
                {
                    Store (API1, Local0)
                    If (LNot (GPIC))
                    {
                        Store (PIC1, Local0)
                    }

                    Return (Local0)
                }
            }

            Scope (\_SB.PCI0.AGP.SLI.SLI0)
            {
                Name (PIC1, Package (0x01)
                {
                    Package (0x04)
                    {
                        0xFFFF, 
                        0x00, 
                        \_SB.PCI0.LNKA, 
                        0x00
                    }
                })
                Name (API1, Package (0x01)
                {
                    Package (0x04)
                    {
                        0xFFFF, 
                        0x00, 
                        0x00, 
                        0x10
                    }
                })
                Method (_PRT, 0, NotSerialized)  // _PRT: PCI Routing Table
                {
                    Store (API1, Local0)
                    If (LNot (GPIC))
                    {
                        Store (PIC1, Local0)
                    }

                    Return (Local0)
                }
            }

            Scope (\_SB.PCI0.AGP.SLI.SLI1)
            {
                Name (PIC1, Package (0x01)
                {
                    Package (0x04)
                    {
                        0xFFFF, 
                        0x00, 
                        \_SB.PCI0.LNKB, 
                        0x00
                    }
                })
                Name (API1, Package (0x01)
                {
                    Package (0x04)
                    {
                        0xFFFF, 
                        0x00, 
                        0x00, 
                        0x11
                    }
                })
                Method (_PRT, 0, NotSerialized)  // _PRT: PCI Routing Table
                {
                    Store (API1, Local0)
                    If (LNot (GPIC))
                    {
                        Store (PIC1, Local0)
                    }

                    Return (Local0)
                }
            }

            Scope (\_SB.PCI0.PCIE)
            {
                Name (PICE, Package (0x03)
                {
                    Package (0x04)
                    {
                        0x0001FFFF, 
                        0x00, 
                        \_SB.PCI0.LNKD, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0x0001FFFF, 
                        0x01, 
                        \_SB.PCI0.LNKC, 
                        0x00
                    }, 

                    Package (0x04)
                    {
                        0xFFFF, 
                        0x00, 
                        \_SB.PCI0.LNKB, 
                        0x00
                    }
                })
                Name (APIE, Package (0x03)
                {
                    Package (0x04)
                    {
                        0x0001FFFF, 
                        0x00, 
                        0x00, 
                        0x13
                    }, 

                    Package (0x04)
                    {
                        0x0001FFFF, 
                        0x01, 
                        0x00, 
                        0x12
                    }, 

                    Package (0x04)
                    {
                        0xFFFF, 
                        0x00, 
                        0x00, 
                        0x11
                    }
                })
                Method (_PRT, 0, NotSerialized)  // _PRT: PCI Routing Table
                {
                    Store (APIE, Local0)
                    If (LNot (GPIC))
                    {
                        Store (PICE, Local0)
                    }

                    Return (Local0)
                }
            }
        }

        Scope (\_SB)
        {
            Device (LID)
            {
                Name (_HID, EisaId ("PNP0C0D") /* Lid Device */)  // _HID: Hardware ID
                Method (_LID, 0, NotSerialized)  // _LID: Lid Status
                {
                    Store (SMI (0x84, 0x00), Local0)
                    Return (Local0)
                }

                Name (_PRW, Package (0x02)  // _PRW: Power Resources for Wake
                {
                    0x17, 
                    0x03
                })
                Method (_PSW, 1, NotSerialized)  // _PSW: Power State Wake
                {
                    PSW (Arg0, 0x02)
                }
            }

            Device (PBTN)
            {
                Name (_HID, EisaId ("PNP0C0C") /* Power Button Device */)  // _HID: Hardware ID
                Name (_PRW, Package (0x02)  // _PRW: Power Resources for Wake
                {
                    0x17, 
                    0x04
                })
                Method (_PSW, 1, NotSerialized)  // _PSW: Power State Wake
                {
                    PSW (Arg0, 0x01)
                }
            }

            Device (SBTN)
            {
                Name (_HID, EisaId ("PNP0C0E") /* Sleep Button Device */)  // _HID: Hardware ID
            }
        }

        Device (AC)
        {
            Name (_HID, "ACPI0003" /* Power Source Device */)  // _HID: Hardware ID
            Name (_PCL, Package (0x02)  // _PCL: Power Consumer List
            {
                \_SB, 
                BAT0
            })
            Method (_PSR, 0, NotSerialized)  // _PSR: Power Source
            {
                And (MIS0, 0x01, Local0)
                Return (Local0)
            }

            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                Return (0x0F)
            }
        }

        Name (BIFP, Package (0x0D) {})
        Method (BIF, 1, NotSerialized)
        {
            SX10 ()
            SX30 (0x01)
            SX30 (Arg0)
            SX11 ()
            Store (SX42 (), Index (BIFP, 0x00))
            Store (SX42 (), Index (BIFP, 0x01))
            Store (SX42 (), Index (BIFP, 0x02))
            Store (SX42 (), Index (BIFP, 0x03))
            Store (SX42 (), Index (BIFP, 0x04))
            Store (SX42 (), Index (BIFP, 0x05))
            Store (SX42 (), Index (BIFP, 0x06))
            Store (SX42 (), Index (BIFP, 0x07))
            Store (SX42 (), Index (BIFP, 0x08))
            Store (SX45 (), Index (BIFP, 0x09))
            Store (SX45 (), Index (BIFP, 0x0A))
            Store (SX45 (), Index (BIFP, 0x0B))
            Store (SX45 (), Index (BIFP, 0x0C))
            SX12 ()
            Return (BIFP) /* \_SB_.BIFP */
        }

        Device (BAT0)
        {
            Name (_HID, EisaId ("PNP0C0A") /* Control Method Battery */)  // _HID: Hardware ID
            Name (_UID, 0x01)  // _UID: Unique ID
            Name (_PCL, Package (0x01)  // _PCL: Power Consumer List
            {
                \_SB
            })
            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                And (MIS0, 0x02, Local0)
                If (Local0)
                {
                    Return (0x1F)
                }

                Return (0x0F)
            }

            Method (_BIF, 0, NotSerialized)  // _BIF: Battery Information
            {
                Return (BIF (0x01))
            }

            Method (_BST, 0, NotSerialized)  // _BST: Battery Status
            {
                SX10 ()
                SX30 (0x02)
                SX30 (0x01)
                SX11 ()
                Name (BST0, Package (0x04) {})
                Store (SX42 (), Index (BST0, 0x00))
                Store (SX42 (), Index (BST0, 0x01))
                Store (SX42 (), Index (BST0, 0x02))
                Store (SX42 (), Index (BST0, 0x03))
                SX12 ()
                Return (BST0) /* \_SB_.BAT0._BST.BST0 */
            }
        }

        Device (MBTN)
        {
            Name (_HID, "PNP0C32")  // _HID: Hardware ID
            Name (_UID, 0x00)  // _UID: Unique ID
            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                If (LLess (OSID (), 0x20))
                {
                    Return (0x00)
                }

                Return (0x0F)
            }

            Name (_PRW, Package (0x02)  // _PRW: Power Resources for Wake
            {
                0x17, 
                0x05
            })
            Method (GHID, 0, NotSerialized)
            {
                Store (HSCO (0x01), Local0)
                If (Local0)
                {
                    Notify (\_SB.MBTN, 0x02) // Device Wake
                    HSCO (0x02)
                }

                Return (Buffer (0x01)
                {
                     0x00                                             /* . */
                })
            }
        }

        Device (AMW0)
        {
            Mutex (WMIX, 0x01)
            Name (_HID, "*pnp0c14")  // _HID: Hardware ID
            Name (_UID, 0x00)  // _UID: Unique ID
            Method (STBY, 3, NotSerialized)
            {
                CreateByteField (Arg0, Arg1, TMP)
                Store (Arg2, TMP) /* \_SB_.AMW0.STBY.TMP_ */
            }

            Method (STWD, 3, NotSerialized)
            {
                CreateWordField (Arg0, Arg1, TMP)
                Store (Arg2, TMP) /* \_SB_.AMW0.STWD.TMP_ */
            }

            Method (STDW, 3, NotSerialized)
            {
                CreateDWordField (Arg0, Arg1, TMP)
                Store (Arg2, TMP) /* \_SB_.AMW0.STDW.TMP_ */
            }

            Method (CLBY, 1, NotSerialized)
            {
                Store (0x00, Local0)
                While (LLess (Local0, SizeOf (Arg0)))
                {
                    STBY (Arg0, Local0, 0x00)
                    Increment (Local0)
                }
            }

            Name (_WDG, Buffer (0x64)
            {
                /* 0000 */  0xBC, 0xDC, 0x9D, 0x8D, 0x97, 0xA9, 0xDA, 0x11,  /* ........ */
                /* 0008 */  0xB0, 0x12, 0xB6, 0x22, 0xA1, 0xEF, 0x54, 0x92,  /* ..."..T. */
                /* 0010 */  0x41, 0x41, 0x01, 0x00, 0xCE, 0x93, 0x05, 0xA8,  /* AA...... */
                /* 0018 */  0x97, 0xA9, 0xDA, 0x11, 0xB0, 0x12, 0xB6, 0x22,  /* ......." */
                /* 0020 */  0xA1, 0xEF, 0x54, 0x92, 0x42, 0x41, 0x01, 0x02,  /* ..T.BA.. */
                /* 0028 */  0x94, 0x59, 0xBB, 0x9D, 0x97, 0xA9, 0xDA, 0x11,  /* .Y...... */
                /* 0030 */  0xB0, 0x12, 0xB6, 0x22, 0xA1, 0xEF, 0x54, 0x92,  /* ..."..T. */
                /* 0038 */  0xD0, 0x00, 0x01, 0x08, 0xE0, 0x6C, 0x77, 0xA3,  /* .....lw. */
                /* 0040 */  0x88, 0x1E, 0xDB, 0x11, 0xA9, 0x8B, 0x08, 0x00,  /* ........ */
                /* 0048 */  0x20, 0x0C, 0x9A, 0x66, 0x42, 0x43, 0x01, 0x00,  /*  ..fBC.. */
                /* 0050 */  0x21, 0x12, 0x90, 0x05, 0x66, 0xD5, 0xD1, 0x11,  /* !...f... */
                /* 0058 */  0xB2, 0xF0, 0x00, 0xA0, 0xC9, 0x06, 0x29, 0x10,  /* ......). */
                /* 0060 */  0x4D, 0x4F, 0x01, 0x00                           /* MO.. */
            })
            Name (INFO, Buffer (0x80) {})
            Name (ECD0, 0x00)
            Method (WED0, 1, NotSerialized)
            {
                Store (Arg0, ECD0) /* \_SB_.AMW0.ECD0 */
                Return (Zero)
            }

            Method (WCAA, 1, NotSerialized)
            {
                Return (Zero)
            }

            Method (WQAA, 1, NotSerialized)
            {
                Acquire (WMIX, 0xFFFF)
                CLBY (INFO)
                If (LNotEqual (Arg0, 0x00))
                {
                    Store (INFO, Local1)
                }
                Else
                {
                    STDW (INFO, 0x00, 0x4C4C4544)
                    STDW (INFO, 0x04, 0x494D5720)
                    STDW (INFO, 0x08, 0x01)
                    STDW (INFO, 0x0C, 0x1000)
                    Store (INFO, Local1)
                }

                Release (WMIX)
                Return (Local1)
            }

            Method (WSAA, 2, NotSerialized)
            {
                Return (Arg1)
            }

            Method (WMBA, 3, NotSerialized)
            {
                CreateDWordField (Arg2, 0x28, WBUF)
                Add (WBUF, 0x2C, Local1)
                If (LLessEqual (Local1, 0x1000))
                {
                    Store (WMI (Arg2, Local1), Local0)
                }

                Return (Local0)
            }

            Method (_WED, 1, NotSerialized)  // _Wxx: Wake Event
            {
                Acquire (WMIX, 0xFFFF)
                CLBY (INFO)
                If (LOr (LNotEqual (Arg0, 0xD0), LEqual (ECD0, 0x00))) {}
                Else
                {
                    SX10 ()
                    SX30 (0x16)
                    SX11 ()
                    Store (0x00, Local2)
                    While (0x01)
                    {
                        Store (SX41 (), Local0)
                        If (LEqual (0x00, Local0))
                        {
                            Break
                        }

                        If (LGreater (Add (Local2, Local0), 0xFF))
                        {
                            Break
                        }

                        STWD (INFO, Local2, Local0)
                        Add (Local2, 0x02, Local2)
                        While (LGreater (Local0, 0x00))
                        {
                            Store (SX41 (), Local1)
                            STWD (INFO, Local2, Local1)
                            Add (Local2, 0x02, Local2)
                            Subtract (Local0, 0x01, Local0)
                        }
                    }

                    SX12 ()
                }

                Release (WMIX)
                Return (INFO) /* \_SB_.AMW0.INFO */
            }

            Name (WQMO, Buffer (0x056F)
            {
                /* 0000 */  0x46, 0x4F, 0x4D, 0x42, 0x01, 0x00, 0x00, 0x00,  /* FOMB.... */
                /* 0008 */  0x5F, 0x05, 0x00, 0x00, 0x88, 0x1C, 0x00, 0x00,  /* _....... */
                /* 0010 */  0x44, 0x53, 0x00, 0x01, 0x1A, 0x7D, 0xDA, 0x54,  /* DS...}.T */
                /* 0018 */  0x18, 0xD5, 0x8D, 0x00, 0x01, 0x06, 0x18, 0x42,  /* .......B */
                /* 0020 */  0x10, 0x0F, 0x10, 0x22, 0x21, 0x04, 0x12, 0x01,  /* ..."!... */
                /* 0028 */  0xA1, 0xC8, 0x2C, 0x0C, 0x86, 0x10, 0x38, 0x2E,  /* ..,...8. */
                /* 0030 */  0x84, 0x1C, 0x40, 0x48, 0x1C, 0x14, 0x4A, 0x08,  /* ..@H..J. */
                /* 0038 */  0x84, 0xFA, 0x13, 0xC8, 0xAF, 0x00, 0x84, 0x0E,  /* ........ */
                /* 0040 */  0x05, 0xC8, 0x14, 0x60, 0x50, 0x80, 0x53, 0x04,  /* ...`P.S. */
                /* 0048 */  0x11, 0xF4, 0x2A, 0xC0, 0xA6, 0x00, 0x93, 0x02,  /* ..*..... */
                /* 0050 */  0x2C, 0x0A, 0xD0, 0x2E, 0xC0, 0xB2, 0x00, 0xDD,  /* ,....... */
                /* 0058 */  0x02, 0xA4, 0xC3, 0x12, 0x91, 0xE0, 0x28, 0x31,  /* ......(1 */
                /* 0060 */  0xE0, 0x28, 0x9D, 0xD8, 0xC2, 0x0D, 0x1B, 0xBC,  /* .(...... */
                /* 0068 */  0x50, 0x14, 0xCD, 0x20, 0x4A, 0x82, 0xCA, 0x05,  /* P.. J... */
                /* 0070 */  0xF8, 0x46, 0x10, 0x78, 0xB9, 0x02, 0x24, 0x4F,  /* .F.x..$O */
                /* 0078 */  0x40, 0x9A, 0x05, 0x18, 0x16, 0x60, 0x5D, 0x80,  /* @....`]. */
                /* 0080 */  0xEC, 0x21, 0x50, 0xA9, 0x43, 0x40, 0xC9, 0x19,  /* .!P.C@.. */
                /* 0088 */  0x02, 0x6A, 0x00, 0xAD, 0x4E, 0x40, 0xF8, 0x95,  /* .j..N@.. */
                /* 0090 */  0x4E, 0x09, 0x49, 0x10, 0xCE, 0x58, 0xC5, 0xE3,  /* N.I..X.. */
                /* 0098 */  0x6B, 0x16, 0x4D, 0xCF, 0x49, 0xCE, 0x31, 0xE4,  /* k.M.I.1. */
                /* 00A0 */  0x78, 0x5C, 0xE8, 0x41, 0xF0, 0x40, 0x0A, 0x40,  /* x\.A.@.@ */
                /* 00A8 */  0x58, 0x78, 0x08, 0x45, 0x80, 0x41, 0x49, 0x18,  /* Xx.E.AI. */
                /* 00B0 */  0x0B, 0x75, 0x31, 0x6A, 0xD4, 0x48, 0xD9, 0x80,  /* .u1j.H.. */
                /* 00B8 */  0x0C, 0x51, 0xDA, 0xA8, 0xD1, 0x03, 0x3A, 0xBF,  /* .Q....:. */
                /* 00C0 */  0x23, 0x39, 0xBB, 0xA3, 0x3B, 0x92, 0x04, 0x46,  /* #9..;..F */
                /* 00C8 */  0x3D, 0xA6, 0x63, 0x2C, 0x6C, 0x46, 0x42, 0x8D,  /* =.c,lFB. */
                /* 00D0 */  0xD1, 0x1C, 0x14, 0x81, 0xC6, 0x0D, 0xDA, 0x12,  /* ........ */
                /* 00D8 */  0x61, 0x35, 0xAE, 0xD8, 0x67, 0x66, 0xE1, 0xC3,  /* a5..gf.. */
                /* 00E0 */  0x12, 0xC6, 0x11, 0x1C, 0x58, 0x82, 0x46, 0xD1,  /* ....X.F. */
                /* 00E8 */  0x34, 0xC7, 0xB3, 0x0D, 0x91, 0xE0, 0x20, 0x42,  /* 4..... B */
                /* 00F0 */  0x63, 0x64, 0x40, 0xC8, 0xF3, 0xB0, 0x05, 0x7A,  /* cd@....z */
                /* 00F8 */  0xE4, 0x09, 0xEC, 0x1E, 0x51, 0x0A, 0x11, 0x34,  /* ....Q..4 */
                /* 0100 */  0xDF, 0x13, 0xA9, 0x51, 0x80, 0x36, 0x0C, 0xD9,  /* ...Q.6.. */
                /* 0108 */  0x3A, 0x1B, 0x68, 0xA8, 0xB1, 0x1A, 0x43, 0x11,  /* :.h...C. */
                /* 0110 */  0x44, 0x84, 0xA0, 0x51, 0x0C, 0x16, 0x21, 0x54,  /* D..Q..!T */
                /* 0118 */  0x88, 0xFF, 0x7F, 0x94, 0xA8, 0xA7, 0x14, 0x24,  /* .......$ */
                /* 0120 */  0x6A, 0x65, 0x20, 0x42, 0x0B, 0x66, 0x04, 0x66,  /* je B.f.f */
                /* 0128 */  0x7F, 0x10, 0x24, 0xC6, 0x99, 0x41, 0x87, 0x05,  /* ..$..A.. */
                /* 0130 */  0xCB, 0x00, 0x91, 0x11, 0x41, 0xA3, 0x61, 0x67,  /* ....A.ag */
                /* 0138 */  0x01, 0x0F, 0xC7, 0x33, 0x69, 0x7E, 0x62, 0x1A,  /* ...3i~b. */
                /* 0140 */  0x9C, 0x09, 0xC6, 0x86, 0x90, 0x06, 0x08, 0x89,  /* ........ */
                /* 0148 */  0x3A, 0x38, 0x50, 0x02, 0x4B, 0x19, 0x38, 0xB1,  /* :8P.K.8. */
                /* 0150 */  0x3D, 0x2E, 0x8D, 0xEF, 0x8C, 0xA3, 0x86, 0x38,  /* =......8 */
                /* 0158 */  0xF5, 0x33, 0xF3, 0x3F, 0xC2, 0x5B, 0xF0, 0x11,  /* .3.?.[.. */
                /* 0160 */  0x80, 0x8F, 0xC1, 0x83, 0x3D, 0x84, 0x80, 0x47,  /* ....=..G */
                /* 0168 */  0xC8, 0xCE, 0x00, 0x06, 0xC4, 0x7B, 0x9F, 0x34,  /* .....{.4 */
                /* 0170 */  0x99, 0x8B, 0xCF, 0x02, 0x30, 0x86, 0x0F, 0xD7,  /* ....0... */
                /* 0178 */  0xF8, 0x28, 0x34, 0x1E, 0x76, 0x3E, 0x60, 0xE3,  /* .(4.v>`. */
                /* 0180 */  0xE2, 0xF0, 0x3E, 0x14, 0x9C, 0x70, 0xB1, 0x20,  /* ..>..p.  */
                /* 0188 */  0x0A, 0x00, 0x21, 0x59, 0xE7, 0x03, 0xF4, 0xAC,  /* ..!Y.... */
                /* 0190 */  0x8F, 0x2D, 0xE0, 0xC3, 0x40, 0xB3, 0x77, 0x08,  /* .-..@.w. */
                /* 0198 */  0x42, 0xF0, 0x22, 0xE0, 0xA3, 0x83, 0x8F, 0x1B,  /* B."..... */
                /* 01A0 */  0x1E, 0xF7, 0xF3, 0x06, 0x18, 0x0E, 0x07, 0x1E,  /* ........ */
                /* 01A8 */  0x8E, 0x4F, 0x1B, 0xC0, 0x65, 0x04, 0x5C, 0xDA,  /* .O..e.\. */
                /* 01B0 */  0x93, 0xC2, 0x04, 0x92, 0xFC, 0x04, 0x90, 0x18,  /* ........ */
                /* 01B8 */  0x18, 0xD4, 0x81, 0xC0, 0x07, 0x0B, 0xB8, 0x92,  /* ........ */
                /* 01C0 */  0xE0, 0x50, 0xC3, 0xF3, 0xC4, 0x1E, 0x10, 0xFE,  /* .P...... */
                /* 01C8 */  0xFF, 0x47, 0x79, 0x22, 0x2F, 0x06, 0x9E, 0xFE,  /* .Gy"/... */
                /* 01D0 */  0x63, 0x00, 0x8C, 0x03, 0x82, 0xA7, 0x75, 0x52,  /* c.....uR */
                /* 01D8 */  0xBE, 0x79, 0x3C, 0x48, 0x78, 0x50, 0x61, 0x12,  /* .y<HxPa. */
                /* 01E0 */  0xF8, 0x94, 0xC0, 0xD0, 0xF8, 0x71, 0x03, 0xAC,  /* .....q.. */
                /* 01E8 */  0xA3, 0xC6, 0x1F, 0x10, 0xE0, 0x9D, 0x24, 0xCE,  /* ......$. */
                /* 01F0 */  0xAF, 0xCF, 0x01, 0xE8, 0xD0, 0x70, 0x8A, 0x0C,  /* .....p.. */
                /* 01F8 */  0xE4, 0x35, 0xE0, 0xA4, 0x4F, 0xC9, 0xE3, 0x4B,  /* .5..O..K */
                /* 0200 */  0xE0, 0x33, 0x07, 0xEC, 0xBB, 0xC1, 0x61, 0x1C,  /* .3....a. */
                /* 0208 */  0x4C, 0x88, 0x08, 0xEF, 0x01, 0x4F, 0x1D, 0xBE,  /* L....O.. */
                /* 0210 */  0x6B, 0x3C, 0x0A, 0x04, 0x8A, 0xD0, 0xDB, 0x99,  /* k<...... */
                /* 0218 */  0x83, 0x9E, 0x42, 0x8C, 0x12, 0xED, 0xAC, 0xC2,  /* ..B..... */
                /* 0220 */  0x3C, 0x70, 0x44, 0xF1, 0x91, 0xC3, 0x08, 0xEF,  /* <pD..... */
                /* 0228 */  0x1E, 0xBE, 0x13, 0x3C, 0x80, 0xB4, 0x36, 0x39,  /* ...<..69 */
                /* 0230 */  0xE1, 0x06, 0x7A, 0xE6, 0x60, 0xD1, 0xCE, 0x2C,  /* ..z.`.., */
                /* 0238 */  0xB2, 0x00, 0xA2, 0x48, 0xA3, 0x41, 0x9D, 0x11,  /* ...H.A.. */
                /* 0240 */  0x7C, 0x1A, 0xF0, 0xB4, 0x9E, 0x62, 0x7C, 0x94,  /* |....b|. */
                /* 0248 */  0x30, 0xC8, 0x19, 0x1E, 0xD8, 0x73, 0xC2, 0x63,  /* 0....s.c */
                /* 0250 */  0x80, 0x07, 0xCC, 0xEE, 0x07, 0x3E, 0x4E, 0xF8,  /* .....>N. */
                /* 0258 */  0x5C, 0x80, 0x77, 0x0D, 0xA8, 0x19, 0xFA, 0xB0,  /* \.w..... */
                /* 0260 */  0x01, 0xE7, 0xD0, 0x81, 0x3F, 0x4D, 0xE0, 0x0F,  /* ....?M.. */
                /* 0268 */  0x16, 0xF8, 0xF1, 0xF8, 0x9A, 0xC3, 0x26, 0x9C,  /* ......&. */
                /* 0270 */  0xC0, 0xF2, 0x07, 0x81, 0x1A, 0x99, 0xA1, 0x3D,  /* .......= */
                /* 0278 */  0xCB, 0xD3, 0x7A, 0x0D, 0xF0, 0x69, 0xC7, 0x04,  /* ..z..i.. */
                /* 0280 */  0x3E, 0x6F, 0xF8, 0xFF, 0xFF, 0xCF, 0xF1, 0x78,  /* >o.....x */
                /* 0288 */  0xC0, 0xAF, 0xF8, 0x74, 0x41, 0xEE, 0x0A, 0x9E,  /* ...tA... */
                /* 0290 */  0xAF, 0xCF, 0x2E, 0xCC, 0xC6, 0x78, 0x50, 0xA3,  /* .....xP. */
                /* 0298 */  0xF0, 0x01, 0x07, 0x77, 0x76, 0xF1, 0x11, 0xC0,  /* ...wv... */
                /* 02A0 */  0x67, 0x17, 0xE0, 0x39, 0x89, 0x67, 0x09, 0xF0,  /* g..9.g.. */
                /* 02A8 */  0x1E, 0x02, 0x7C, 0x22, 0x89, 0xF7, 0xB0, 0x05,  /* ..|".... */
                /* 02B0 */  0x63, 0xC4, 0x78, 0xC8, 0x33, 0xAE, 0x7A, 0x18,  /* c.x.3.z. */
                /* 02B8 */  0xBA, 0x08, 0x58, 0xDD, 0x7D, 0x05, 0x75, 0xF4,  /* ..X.}.u. */
                /* 02C0 */  0x02, 0x13, 0xD4, 0x6B, 0x06, 0xEE, 0xF4, 0x02,  /* ...k.... */
                /* 02C8 */  0x7C, 0x4E, 0x59, 0xF0, 0xFE, 0xFF, 0xA7, 0x2C,  /* |NY...., */
                /* 02D0 */  0xE0, 0x7E, 0x55, 0xE0, 0x47, 0x14, 0x30, 0x40,  /* .~U.G.0@ */
                /* 02D8 */  0x76, 0x76, 0x3A, 0x11, 0xC2, 0x7B, 0xC9, 0x73,  /* vv:..{.s */
                /* 02E0 */  0x88, 0x6F, 0x57, 0x3E, 0x98, 0x04, 0x79, 0x0E,  /* .oW>..y. */
                /* 02E8 */  0x88, 0xF0, 0x94, 0xC5, 0xEF, 0x03, 0x51, 0x62,  /* ......Qb */
                /* 02F0 */  0x1E, 0x50, 0xA4, 0x28, 0x46, 0x0C, 0xF2, 0x84,  /* .P.(F... */
                /* 02F8 */  0xE5, 0xEB, 0x49, 0x0C, 0x43, 0x07, 0x0B, 0x17,  /* ..I.C... */
                /* 0300 */  0x3E, 0xC2, 0x53, 0x16, 0x60, 0xF1, 0x92, 0x85,  /* >.S.`... */
                /* 0308 */  0x39, 0x65, 0xC1, 0x7C, 0x1B, 0xF8, 0x94, 0x05,  /* 9e.|.... */
                /* 0310 */  0x8E, 0xFF, 0xFF, 0x29, 0x0B, 0x5C, 0xE3, 0x7E,  /* ...).\.~ */
                /* 0318 */  0xCA, 0x02, 0x66, 0xD2, 0x9F, 0x02, 0x3E, 0xD5,  /* ..f...>. */
                /* 0320 */  0xF8, 0x09, 0xA0, 0xE8, 0x07, 0x0B, 0x0A, 0xE3,  /* ........ */
                /* 0328 */  0x53, 0x16, 0xE0, 0x4A, 0xDE, 0x01, 0x01, 0x34,  /* S..J...4 */
                /* 0330 */  0x67, 0x27, 0xDF, 0x16, 0x0C, 0x76, 0xCC, 0xBE,  /* g'...v.. */
                /* 0338 */  0x64, 0xF8, 0x94, 0x08, 0x86, 0x43, 0x86, 0xEF,  /* d....C.. */
                /* 0340 */  0x54, 0x87, 0xF2, 0xC8, 0xF1, 0x14, 0xE0, 0x23,  /* T......# */
                /* 0348 */  0x16, 0xD8, 0xE3, 0x1C, 0x03, 0x74, 0x5C, 0xF1,  /* .....t\. */
                /* 0350 */  0x11, 0xCB, 0xFF, 0xFF, 0x23, 0x16, 0xC0, 0x8D,  /* ....#... */
                /* 0358 */  0x03, 0x08, 0xFE, 0xD4, 0x01, 0xEB, 0x1E, 0x10,  /* ........ */
                /* 0360 */  0xD6, 0x87, 0x0E, 0xE0, 0x21, 0xFB, 0x21, 0xA0,  /* ....!.!. */
                /* 0368 */  0x33, 0x8C, 0x25, 0x83, 0xC8, 0xC6, 0xB9, 0x86,  /* 3.%..... */
                /* 0370 */  0x8E, 0xD1, 0xE2, 0x17, 0xAA, 0x9B, 0x42, 0xEC,  /* ......B. */
                /* 0378 */  0x83, 0xE1, 0xB2, 0x81, 0x04, 0xEA, 0xE1, 0x5A,  /* .......Z */
                /* 0380 */  0x30, 0x85, 0x44, 0xD1, 0x68, 0x34, 0x06, 0x26,  /* 0.D.h4.& */
                /* 0388 */  0x30, 0x82, 0x33, 0x88, 0x01, 0x9D, 0x11, 0x42,  /* 0.3....B */
                /* 0390 */  0x87, 0x32, 0x9C, 0x8A, 0xF3, 0x10, 0xEA, 0xFF,  /* .2...... */
                /* 0398 */  0x4F, 0x30, 0xD4, 0x8D, 0x89, 0xCE, 0xCE, 0xF3,  /* O0...... */
                /* 03A0 */  0xE7, 0xB7, 0x11, 0x9F, 0x08, 0x0C, 0xEC, 0x2B,  /* .......+ */
                /* 03A8 */  0xC4, 0x5B, 0x06, 0x58, 0x86, 0xE5, 0xC5, 0x3D,  /* .[.X...= */
                /* 03B0 */  0x01, 0x1C, 0xE3, 0x49, 0x26, 0xA8, 0xE6, 0x58,  /* ...I&..X */
                /* 03B8 */  0x83, 0x9A, 0x83, 0xAF, 0x02, 0x6F, 0x64, 0x26,  /* .....od& */
                /* 03C0 */  0xF0, 0x15, 0x0C, 0x6C, 0xA7, 0x19, 0x8C, 0xBE,  /* ...l.... */
                /* 03C8 */  0x3B, 0x01, 0x28, 0x80, 0x7C, 0x14, 0xF0, 0x7D,  /* ;.(.|..} */
                /* 03D0 */  0xF9, 0x6D, 0x80, 0xCD, 0xE2, 0x95, 0xD9, 0x68,  /* .m.....h */
                /* 03D8 */  0x3E, 0x7F, 0x22, 0x86, 0x8E, 0x12, 0x33, 0x74,  /* >."...3t */
                /* 03E0 */  0x0A, 0xE2, 0xA1, 0x3B, 0xE8, 0xD0, 0xD1, 0xC7,  /* ...;.... */
                /* 03E8 */  0x01, 0x9F, 0xAC, 0x70, 0xC1, 0x0E, 0x5F, 0xD0,  /* ...p.._. */
                /* 03F0 */  0x26, 0x77, 0xB4, 0x27, 0xE6, 0x59, 0x78, 0x9E,  /* &w.'.Yx. */
                /* 03F8 */  0xB8, 0xB9, 0x83, 0xE9, 0x88, 0x04, 0x63, 0xF0,  /* ......c. */
                /* 0400 */  0x98, 0xC9, 0x83, 0x59, 0xE0, 0xE4, 0x41, 0xF1,  /* ...Y..A. */
                /* 0408 */  0xFF, 0x9F, 0x3C, 0x4C, 0x78, 0x4C, 0xD8, 0xC3,  /* ..<LxL.. */
                /* 0410 */  0x21, 0x3D, 0x74, 0x78, 0x64, 0x7C, 0x9C, 0x3E,  /* !=txd|.> */
                /* 0418 */  0xFD, 0x30, 0xEC, 0xD3, 0x39, 0x97, 0xA2, 0x67,  /* .0..9..g */
                /* 0420 */  0xA4, 0x3B, 0xC6, 0x33, 0x17, 0x06, 0xD6, 0x23,  /* .;.3...# */
                /* 0428 */  0xE7, 0xB0, 0x46, 0x0B, 0x7B, 0xC0, 0xCF, 0x21,  /* ..F.{..! */
                /* 0430 */  0xBE, 0xC3, 0xF8, 0xC0, 0xC3, 0x60, 0x7D, 0x7A,  /* .....`}z */
                /* 0438 */  0x01, 0xC7, 0xF1, 0x0B, 0xFE, 0x69, 0x00, 0x3C,  /* .....i.< */
                /* 0440 */  0x07, 0x10, 0x8F, 0xE1, 0x05, 0x84, 0x1F, 0x5F,  /* ......._ */
                /* 0448 */  0x74, 0xFE, 0xA5, 0x42, 0x17, 0x27, 0x79, 0x30,  /* t..B.'y0 */
                /* 0450 */  0xA8, 0xD3, 0x14, 0xE0, 0xEA, 0xF4, 0x06, 0x9E,  /* ........ */
                /* 0458 */  0xAB, 0x3E, 0xEE, 0x2C, 0x85, 0xFB, 0xFF, 0x9F,  /* .>.,.... */
                /* 0460 */  0xA5, 0x60, 0x1C, 0x7E, 0x7D, 0x81, 0xF7, 0x5D,  /* .`.~}..] */
                /* 0468 */  0xCA, 0x08, 0x07, 0xF2, 0x2C, 0x05, 0xF6, 0xD8,  /* ....,... */
                /* 0470 */  0xCF, 0x10, 0x1D, 0x02, 0x7C, 0x96, 0x02, 0xF8,  /* ....|... */
                /* 0478 */  0xF1, 0xFA, 0xF0, 0x79, 0x06, 0x6E, 0xE0, 0xD3,  /* ...y.n.. */
                /* 0480 */  0x2F, 0xD0, 0xFA, 0xFF, 0x9F, 0x42, 0xC0, 0x7F,  /* /....B.. */
                /* 0488 */  0x8C, 0xF0, 0x11, 0x07, 0x77, 0xF4, 0x05, 0x6E,  /* ....w..n */
                /* 0490 */  0x07, 0x63, 0x7E, 0xC6, 0xC0, 0x1D, 0xC7, 0x80,  /* .c~..... */
                /* 0498 */  0xC7, 0xC9, 0x19, 0x77, 0x9A, 0xF0, 0x10, 0xF8,  /* ...w.... */
                /* 04A0 */  0x00, 0x5A, 0x9D, 0x1E, 0x39, 0xF5, 0x9C, 0x12,  /* .Z..9... */
                /* 04A8 */  0xEE, 0x38, 0xC0, 0xA7, 0x84, 0x1B, 0x00, 0x26,  /* .8.....& */
                /* 04B0 */  0xC0, 0x19, 0x13, 0x50, 0xF5, 0xFF, 0x3F, 0x63,  /* ...P..?c */
                /* 04B8 */  0x02, 0x63, 0x90, 0x08, 0xEF, 0x0A, 0x51, 0xDF,  /* .c....Q. */
                /* 04C0 */  0x91, 0x0D, 0xF1, 0x8C, 0xCC, 0x70, 0xDE, 0x1A,  /* .....p.. */
                /* 04C8 */  0x1E, 0x5F, 0x9E, 0x91, 0xC1, 0x79, 0xC6, 0x04,  /* ._...y.. */
                /* 04D0 */  0xF8, 0xF3, 0xFF, 0x3F, 0xD2, 0x63, 0x6F, 0x0A,  /* ...?.co. */
                /* 04D8 */  0xC1, 0x9F, 0x42, 0x80, 0x89, 0xF8, 0x33, 0x26,  /* ..B...3& */
                /* 04E0 */  0x15, 0x7E, 0xC6, 0x84, 0x36, 0x17, 0x4F, 0xDB,  /* .~..6.O. */
                /* 04E8 */  0x67, 0x4C, 0x80, 0x7B, 0xFF, 0xFF, 0x33, 0x26,  /* gL.{..3& */
                /* 04F0 */  0xE0, 0x3B, 0xF2, 0x19, 0x13, 0xD0, 0x73, 0xA4,  /* .;....s. */
                /* 04F8 */  0xC2, 0x9D, 0x31, 0xC1, 0x30, 0xCE, 0xF7, 0x27,  /* ..1.0..' */
                /* 0500 */  0x83, 0x3C, 0x8F, 0xF8, 0x40, 0xE0, 0xA3, 0x0C,  /* .<..@... */
                /* 0508 */  0x53, 0x68, 0xD3, 0xA7, 0x46, 0xA3, 0x56, 0x0D,  /* Sh..F.V. */
                /* 0510 */  0xCA, 0xD4, 0x28, 0xD3, 0xA0, 0x56, 0x9F, 0x4A,  /* ..(..V.J */
                /* 0518 */  0x8D, 0x19, 0x3B, 0x3A, 0x59, 0xC4, 0x5A, 0x35,  /* ..;:Y.Z5 */
                /* 0520 */  0x58, 0x87, 0x5A, 0xAF, 0x40, 0x2C, 0xE9, 0x89,  /* X.Z.@,.. */
                /* 0528 */  0x21, 0x10, 0xFF, 0xFF, 0x45, 0x79, 0x00, 0x61,  /* !...Ey.a */
                /* 0530 */  0x71, 0x4D, 0x80, 0x30, 0xE1, 0xAB, 0x12, 0x88,  /* qM.0.... */
                /* 0538 */  0x63, 0x83, 0x50, 0xB1, 0x3A, 0x20, 0x1A, 0x19,  /* c.P.: .. */
                /* 0540 */  0xA2, 0x41, 0x04, 0xE4, 0x10, 0x3E, 0x80, 0x58,  /* .A...>.X */
                /* 0548 */  0x24, 0x10, 0x81, 0x13, 0x25, 0xA0, 0xD4, 0x09,  /* $...%... */
                /* 0550 */  0x08, 0x13, 0xBA, 0x16, 0x81, 0x58, 0x9E, 0x17,  /* .....X.. */
                /* 0558 */  0x10, 0x16, 0x0E, 0x84, 0x4A, 0x32, 0x03, 0xCE,  /* ....J2.. */
                /* 0560 */  0xF4, 0x81, 0x08, 0xC8, 0x52, 0x5F, 0x21, 0x02,  /* ....R_!. */
                /* 0568 */  0xB2, 0x70, 0x10, 0x01, 0xF9, 0xFF, 0x0F         /* .p..... */
            })
        }
    }
}

